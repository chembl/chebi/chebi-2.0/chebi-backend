from django.contrib.auth.decorators import login_required
from django.urls import path

from .api_validation_views import (
    ValidateChebiName,
    ValidateSynonym,
    HtmxSubmitCompoundGetSources,
    GetChemicalDataFromMol,
    GetMassFromFormula,
    SpeciesSearch,
    ComponentSearch,
    GetSourceAccessionField,
)
from .views.profile import SubmitterHomeView, SubmitterProfileView
from .views.retrieve_compound import SubmitterEntityDetailView, SubmitterEntityListView
from .views.submit_compound import SubmitCompoundView
from .views.update_compound import (
    NamesUpdateView,
    HtmxUpdateCompoundGetSourceView,
    SubmitterCreateNameView,
    CompoundUpdateView,
    SubmitterCreateCrossReferenceView,
    SubmitterUpdateCrossReferenceView,
)

app_name = "submitter"

# All submitter views require login
# TODO: Can we move login_required decorator to base.urls?
urlpatterns = [
    path("", login_required(SubmitterHomeView.as_view()), name="dashboard"),
    path("new/", login_required(SubmitCompoundView.as_view()), name="submit-new"),
    path("profile/", login_required(SubmitterProfileView.as_view()), name="profile"),
    path("CHEBI:<pk>/", login_required(SubmitterEntityDetailView.as_view()), name="entity-detail"),
    path("CHEBI:<pk>/edit/", login_required(CompoundUpdateView.as_view()), name="edit-entity"),
    path("name/<pk>/update/", login_required(NamesUpdateView.as_view()), name="name-update"),
    path("CHEBI:<pk>/add-synonym/", login_required(SubmitterCreateNameView.as_view()), name="add-synonym"),
    path("CHEBI:<pk>/add-xref/", login_required(SubmitterCreateCrossReferenceView.as_view()), name="add-xref"),
    path("xref/<pk>/edit/", login_required(SubmitterUpdateCrossReferenceView.as_view()), name="update-xref"),
    path("my-submissions/", login_required(SubmitterEntityListView.as_view()), name="my-submissions"),
    # HTMX Views
    path("validate/name/", login_required(ValidateChebiName.as_view()), name="validate_chebi_name"),
    path(
        "validate/source/<str:form_type>/",
        login_required(HtmxSubmitCompoundGetSources.as_view()),
        name="validate_source",
    ),
    path("validate/synonym/", login_required(ValidateSynonym.as_view()), name="validate_synonym"),
    path(
        "validate/mol_to_chemical_data/", login_required(GetChemicalDataFromMol.as_view()), name="mol_to_chemical_data"
    ),
    path("validate/formula_to_mass/", login_required(GetMassFromFormula.as_view()), name="formula_to_mass"),
    path("validate/species/", login_required(SpeciesSearch.as_view()), name="species_search"),
    path("validate/component/", login_required(ComponentSearch.as_view()), name="component_search"),
    path(
        "validate/source_accession/",
        login_required(GetSourceAccessionField.as_view()),
        name="get_source_accession_field",
    ),
    path("htmx/get/source/", login_required(HtmxUpdateCompoundGetSourceView.as_view()), name="htmx_get_source"),
]
