from libRDChEBI.descriptors import (
    get_molformula,
    get_net_charge,
    get_avg_mass,
    get_monoisotopic_mass,
    get_mass_from_formula,
)
from libRDChEBI.formats import get_smiles
from rdkit import Chem

from chebi_models.models import Status


def save_names_formset(names, compound, status, created_by):
    """
    Iterates over each Name record of NamesFormset and saves it
    """
    for name in names:
        name.compound = compound
        name.status = status
        name.created_by = created_by
        name.save()


def save_compound_origins_formset(compound_origins, compound, status, created_by):
    """
    Iterates over each record of CompoundOrigins and saves it
    """
    for origin in compound_origins:
        origin.compound = compound
        origin.status = status
        origin.created_by = created_by
        origin.save()


def save_database_accessions_formset(database_accessions, compound, status, created_by):
    """
    Iterates over each DatabaseAccession record of DatabaseAccessionFormset and saves it
    """
    for accession in database_accessions:
        accession.compound = compound
        accession.status = status
        accession.created_by = created_by
        accession.save()


def is_molfile_empty(mol_block):
    """
    Returns boolean indicating if a mol file is empty
    Molfile can be empty if user clears the input in sketcher, it still contains a molfile but that's empty
    """
    mol = Chem.MolFromMolBlock(mol_block, sanitize=False, removeHs=False)
    return mol is None or mol.GetNumAtoms() == 0


def get_chemical_data_from_molfile(mol):
    inchi = Chem.MolBlockToInchi(mol)
    return {
        "formula": get_molformula(mol),
        "charge": get_net_charge(mol),
        "mass": round(get_avg_mass(mol), 3),
        "monoisotopic_mass": round(get_monoisotopic_mass(mol), 5),
        "inchi": inchi if inchi else None,
        "inchi_key": Chem.InchiToInchiKey(inchi) if inchi else None,
        "smiles": get_smiles(mol),
    }


def mass_from_formula(formula):
    return {
        "mass": round(get_mass_from_formula(formula, average=True), 3),
        "monoisotopic_mass": round(get_mass_from_formula(formula, average=False), 5),
    }


def get_default_submitter_status():
    """
    Anything that is created in submitter portal has default status of "Submitted"
    """
    return Status.objects.get(code="S")
