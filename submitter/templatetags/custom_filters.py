from django import template

register = template.Library()


@register.filter(name="add_class")
def add_class(value, arg):
    # Get existing classes from the widget's attrs, if any
    # Combine the existing classes with the new one
    existing_classes = value.field.widget.attrs.get("class", "")
    combined_classes = f"{existing_classes} {arg}".strip()
    return value.as_widget(attrs={"class": combined_classes})


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key) if dictionary else None


@register.simple_tag
def url_replace(request, field, value):
    """
    Custom tag that preserves existing query parameters when changing page numbers
    """
    dict_ = request.GET.copy()
    dict_[field] = value
    return dict_.urlencode()


@register.filter
def format_component_accession(value):
    """
    Used with component accession string. Replaces : with _ in that
    """
    return value.replace(":", "_")
