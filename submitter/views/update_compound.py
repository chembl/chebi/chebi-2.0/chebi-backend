from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse
from django.template import Template, Context
from django.urls import reverse_lazy
from django.views.generic import UpdateView, CreateView
from django.db.models import Q
from rest_framework.views import APIView

from chebi_models.models import Names, Compound, DatabaseAccession

from submitter.forms.update_forms import (
    UpdateNamesForm,
    DynamicSourceForm,
    SubmitterCreateNamesForm,
    UpdateCompoundForm,
    SubmitterCreateXRefForm,
    SubmitterUpdateXRefForm,
)


class SubmitterGenericUpdateView(UpdateView):
    """
    Generic update view for Submitter
    Hold the common logic used by Names, Database Accessions and Compound Origin update views
    """

    def get_queryset(self):
        qs = super().get_queryset()
        user = self.request.user

        # Filter the relevant records where:
        # 1. The compound itself was created by the user
        # 2. Any child compound was created by the user
        # 3. The compound's parent was created by the user
        return qs.filter(
            compound__in=Compound.objects.filter(
                Q(created_by=user) | Q(children__created_by=user) | Q(parent__created_by=user)
            )
        )

    def get_success_url(self):
        return reverse_lazy("submitter:entity-detail", kwargs={"pk": self.object.compound.pk})

    def _can_edit(self):
        """
        Returns bool if submitter can edit an entry.
        Submitter can only edit an entry if:
            - Its status is either OK or Submitted
            - The parent compound is not 3-star
        """
        parent_compound = self.object.compound.parent if self.object.compound.parent else self.object.compound
        return self.object.status.code in ["E", "S"] and parent_compound.stars < 3

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self._can_edit():
            messages.warning(
                request,
                "This data cannot be edited. please contact ChEBI curators to make any changes.",
            )
        return self.render_to_response(self.get_context_data(can_edit=self._can_edit()))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self._can_edit():
            messages.error(request, "Error: Cannot make any changes to this data.")
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.render_to_response(self.get_context_data(form=form, can_edit=self._can_edit()))


class HtmxUpdateCompoundGetSourceView(APIView):
    def get(self, request):
        source_type = request.GET.get("type", None)
        htmx_source_field = bool(request.GET.get("htmx_source_field", False))
        source_accession_field_name = request.GET.get("source_accession_field_name", None)
        form = DynamicSourceForm(
            source_type=source_type,
            htmx_source_field=htmx_source_field,
            source_accession_field_name=source_accession_field_name,
        )
        return HttpResponse(Template("{{ form.source.as_field_group }}").render(Context({"form": form})))


class NamesUpdateView(SuccessMessageMixin, SubmitterGenericUpdateView):
    model = Names
    form_class = UpdateNamesForm
    success_message = "Name updated successfully."
    template_name = "submitter/compound-update/name/edit_name.html"


class SubmitterCreateNameView(CreateView):
    model = Names
    form_class = SubmitterCreateNamesForm
    template_name = "submitter/compound-update/name/create_name.html"

    def get_success_url(self):
        return reverse_lazy("submitter:entity-detail", kwargs={"pk": self.object.compound.pk})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["compound_id"] = self.kwargs.get("pk")
        kwargs["created_by_id"] = self.request.user.pk
        return kwargs


class CompoundUpdateView(SuccessMessageMixin, UpdateView):
    model = Compound
    form_class = UpdateCompoundForm
    success_message = "Compound details updated successfully."
    template_name = "submitter/compound-update/compound.html"

    def get_queryset(self):
        """
        Can only edit if user submitted this compound or any of its child compounds
        """
        user = self.request.user
        return super().get_queryset().filter(Q(created_by=user) | Q(children__created_by=user))

    def get_success_url(self):
        return reverse_lazy("submitter:entity-detail", kwargs={"pk": self.object.pk})

    def _can_edit(self):
        """
        Submitter cannot edit details of Compound after it has been fully curated (i.e. is 3-star)
        """
        return self.object.stars < 3

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self._can_edit():
            messages.warning(
                request,
                "This data cannot be edited. please contact ChEBI curators to make any changes.",
            )
        return self.render_to_response(self.get_context_data(can_edit=self._can_edit()))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self._can_edit():
            messages.error(request, "Error: Cannot make any changes to this data.")
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.render_to_response(self.get_context_data(form=form, can_edit=self._can_edit()))


class SubmitterCreateCrossReferenceView(CreateView):
    model = DatabaseAccession
    form_class = SubmitterCreateXRefForm
    template_name = "submitter/compound-update/xref/create_xref.html"

    def get_success_url(self):
        return reverse_lazy("submitter:entity-detail", kwargs={"pk": self.object.compound.pk})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["compound_id"] = self.kwargs.get("pk")
        kwargs["created_by_id"] = self.request.user.pk
        return kwargs


class SubmitterUpdateCrossReferenceView(SuccessMessageMixin, SubmitterGenericUpdateView):
    model = DatabaseAccession
    form_class = SubmitterUpdateXRefForm
    success_message = "Cross-reference updated successfully."
    template_name = "submitter/compound-update/xref/update_xref.html"
