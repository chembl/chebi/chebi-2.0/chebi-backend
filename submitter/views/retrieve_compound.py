from django.contrib import messages
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView
from libRDChEBI.depiction import depict
from django.db.models import Q

from chebi_models.models import Compound


class SubmitterEntityListView(ListView):
    model = Compound
    template_name = "submitter/compound_list.html"
    paginate_by = 20
    ordering = "-pk"

    def get_queryset(self):
        qs = super().get_queryset().filter(created_by=self.request.user)
        search_keyword = self.request.GET.get("keyword")
        if search_keyword:
            qs = qs.filter(Q(name__icontains=search_keyword) | Q(chebi_accession__icontains=search_keyword))
        return qs


class SubmitterEntityDetailView(DetailView):
    model = Compound
    template_name = "submitter/compound-detail/index.html"

    def get_object(self, queryset=None):
        # Return parent object if it exists
        compound_obj = get_object_or_404(Compound.objects.all(), pk=self.kwargs.get("pk"))

        if (
            compound_obj.created_by != self.request.user
            and not compound_obj.children.filter(created_by=self.request.user).exists()
        ):
            # Return 404 if the current user did not create this entry or any of its children
            raise Http404

        if compound_obj.parent:
            messages.info(
                self.request,
                f"{compound_obj.chebi_accession} has been merged by ChEBI curators. You are now viewing details of "
                f"its parent entry.",
            )
            return compound_obj.parent
        return compound_obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["submitters"] = (
            Compound.objects.filter(pk__in=self.object.compound_with_children_ids, created_by__is_anonymous=False)
            .values_list("created_by__public_name", flat=True)
            .distinct()
        )
        context["compound_with_children"] = [self.object] + list(self.object.children.all().order_by("pk"))
        if self.object.default_structure:
            context["default_structure_svg"] = depict(
                self.object.default_structure.molfile, maxFontSize=50, width=400, height=400
            )
        return context
