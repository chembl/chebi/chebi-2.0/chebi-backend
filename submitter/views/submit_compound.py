from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic import FormView

from chebi_models.models import Compound, Names, DatabaseAccession, CompoundOrigins

from submitter.forms.submit_forms import (
    SubmitCompoundForm,
    NamesFormSet,
    DatabaseAccessionFormSet,
    ChemicalDataForm,
    CompoundOriginsFormSet,
)
from submitter.utils import (
    save_names_formset,
    save_database_accessions_formset,
    save_compound_origins_formset,
    get_default_submitter_status,
)


class SubmitCompoundView(FormView):
    model = Compound
    template_name = "submitter/submit.html"
    form_class = SubmitCompoundForm
    formset_class = NamesFormSet

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chemical_data_form = None
        self.names_formset = None
        self.database_accession_formset = None
        self.compound_origins_formset = None

    def form_valid(self, form):
        """If the form is valid, redirect to entry detail page"""
        messages.success(self.request, "Entry has been submitted successfully")
        return redirect("submitter:entity-detail", pk=form.instance.pk)

    def get_formsets(self, data=None):
        """Initialize formsets with or without data."""
        self.chemical_data_form = ChemicalDataForm(data) if data else ChemicalDataForm()
        self.names_formset = (
            NamesFormSet(data, prefix="names") if data else NamesFormSet(queryset=Names.objects.none(), prefix="names")
        )
        self.database_accession_formset = (
            DatabaseAccessionFormSet(data, prefix="database-accession")
            if data
            else DatabaseAccessionFormSet(queryset=DatabaseAccession.objects.none(), prefix="database-accession")
        )
        self.compound_origins_formset = (
            CompoundOriginsFormSet(data, prefix="compound-origins")
            if data
            else CompoundOriginsFormSet(queryset=CompoundOrigins.objects.none(), prefix="compound-origins")
        )

    def validate_all_forms(self, *forms):
        all_valid = True
        for form in forms:
            if not form.is_valid():
                all_valid = False
        return all_valid

    def get(self, request, *args, **kwargs):
        self.get_formsets(data=None)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        form = self.get_form()
        self.get_formsets(request.POST)
        if self.validate_all_forms(
            form,
            self.names_formset,
            self.database_accession_formset,
            self.chemical_data_form,
            self.compound_origins_formset,
        ):
            # If form and all nested forms are valid, then create the compound
            compound = form.save()
            # Initialise instances for nested forms
            database_accessions = self.database_accession_formset.save(commit=False)
            names = self.names_formset.save(commit=False)
            compound_origins = self.compound_origins_formset.save(commit=False)
            default_status = get_default_submitter_status()
            # Save nested formsets
            save_names_formset(names=names, compound=compound, status=default_status, created_by=request.user)
            save_compound_origins_formset(
                compound_origins=compound_origins, compound=compound, status=default_status, created_by=request.user
            )
            save_database_accessions_formset(
                database_accessions=database_accessions,
                compound=compound,
                status=default_status,
                created_by=request.user,
            )
            # Extract molfile of default structure and pass it to save_chemical_data
            self.chemical_data_form.save_with_structure(
                compound=compound,
                status=default_status,
                created_by=request.user,
                molfile=self.chemical_data_form.data.get("mol_default_structure"),
            )

            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({"chemical_data_form": self.chemical_data_form})
        kwargs.update({"names_formset": self.names_formset})
        kwargs.update({"database_accession_formset": self.database_accession_formset})
        kwargs.update({"compound_origins_formset": self.compound_origins_formset})
        return kwargs
