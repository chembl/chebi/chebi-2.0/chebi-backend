from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import TemplateView, UpdateView

from chebi_models.models import User

from submitter.forms.update_forms import (
    SubmitterProfileForm,
)


class SubmitterHomeView(TemplateView):
    template_name = "submitter/dashboard.html"


class SubmitterProfileView(SuccessMessageMixin, UpdateView):
    model = User
    form_class = SubmitterProfileForm
    template_name = "submitter/profile.html"
    success_url = reverse_lazy("submitter:profile")
    success_message = "Profile has been updated"

    def get_object(self, queryset=None):
        return self.request.user
