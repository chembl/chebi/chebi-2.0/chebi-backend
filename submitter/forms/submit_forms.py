from datetime import date, timedelta

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.forms import modelformset_factory
from django.db.models import Q
from django.urls import reverse_lazy
from libRDChEBI.descriptors import has_r_group


from chebi_models.models import (
    Compound,
    Names,
    DatabaseAccession,
    ChemicalData,
    Structure,
    Source,
    CompoundOrigins,
)
from chebi_models.models.choices import NameTypeSubmitterFormChoices, DatabaseAccessionTypeChoices
from submitter.utils import is_molfile_empty, get_chemical_data_from_molfile, mass_from_formula
from submitter.widgets import Select2SpeciesWidget, Select2ComponentWidget


class SubmitCompoundForm(forms.ModelForm):
    class Meta:
        model = Compound
        fields = ["name", "definition", "release_date", "submitter_notes"]
        labels = {
            "name": "ChEBI Name",
        }
        widgets = {
            "name": forms.Textarea(
                attrs={
                    "rows": 2,
                    "hx-get": reverse_lazy("submitter:validate_chebi_name"),
                    "hx-target": "closest div",
                    "hx-swap": "outerHTML",
                    "hx-indicator": "#htmx-loader",
                    "hx-trigger": "keyup changed delay:700ms",
                }
            ),
            "definition": forms.Textarea(attrs={"rows": 2}),
            "submitter_notes": forms.Textarea(attrs={"rows": 2}),
            # Show release date from tomorrow onwards
            "release_date": forms.DateInput(attrs={"min": date.today() + timedelta(days=1), "type": "date"}),
        }

    def __init__(self, *args, **kwargs):
        """
        Override to add additional attributes i.e. user, success_message and warning_message
        """
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)
        self.success_message = {}
        self.warning_message = {}

    def clean_name(self):
        name = self.cleaned_data.get("name")

        existing_compound = Compound.pre_check_existing_name(name)
        if existing_compound:
            raise ValidationError(
                f"Cannot submit new entry, <a href='{settings.FRONTEND_APP_URL}{existing_compound.chebi_accession}' target='_blank'>{existing_compound.chebi_accession}</a> already exists under this name. Please reach out to the ChEBI team to update or add more details to the existing entry."
            )

        similar_names = Names.pre_check_similar_names(name)
        if similar_names:
            compound_links = [
                (
                    f"<a href='{settings.FRONTEND_APP_URL}{name.compound.chebi_accession}' "
                    f"target='_blank'>{name.compound.chebi_accession}</a>"
                )
                for name in similar_names
            ]
            self.warning_message["name"] = (
                f"This name is currently listed as a synonym in ChEBI, which may suggest a duplication. "
                f"Please review {', '.join(compound_links)} to ensure there is no overlap, or contact the "
                f"ChEBI team if unsure."
            )
        else:
            self.success_message["name"] = (
                "This name is unique and has been successfully validated. You may proceed with your submission."
            )
        return name

    def clean_release_date(self):
        release_date = self.cleaned_data.get("release_date")
        if release_date and date.today() > release_date:
            raise ValidationError(
                "Please provide a future release date, or leave this field blank for your "
                "submission to be released publicly now."
            )
        return release_date

    def clean(self):
        cleaned_data = super().clean()
        if not (cleaned_data.get("definition") or self.data.get("mol_default_structure")):
            raise ValidationError(
                "Along with ChEBI Name, at-least a definition or a structure is required for submission."
            )
        return cleaned_data

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.ascii_name = self.cleaned_data.get("name")
        instance.created_by = self.user
        # If release date exists, it must be already validated by clean_release_date
        instance.is_released = False if instance.release_date else True
        if commit:
            instance.save()
        return instance


class NamesForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "hx-get": reverse_lazy("submitter:validate_synonym"),
                "hx-target": "closest .synonym-name-container",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
                "hx-trigger": "keyup changed delay:700ms",
            }
        )
    )
    type = forms.ChoiceField(
        choices=NameTypeSubmitterFormChoices.choices,
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:validate_source", kwargs={"form_type": "name"}),
                "hx-target": "next .synonym-source-container",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
            }
        ),
    )
    source = forms.ModelChoiceField(
        queryset=Source.objects.filter(
            Q(is_synonym_source=True) | Q(is_inn_source=True) | Q(is_iupac_source=True) | Q(is_brand_name_source=True)
        )
    )

    class Meta:
        model = Names
        fields = ["name", "type", "source", "language_code"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.warning_message = {}

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.ascii_name = self.cleaned_data.get("name")
        if commit:
            instance.save()
        return instance

    def clean_name(self):
        name = self.cleaned_data.get("name")

        similar_names = Names.pre_check_similar_names(name)
        if similar_names:
            compound_links = [
                (
                    f"<a href='{settings.FRONTEND_APP_URL}{name.compound.chebi_accession}' "
                    f"target='_blank'>{name.compound.chebi_accession}</a>"
                )
                for name in similar_names
            ]
            self.warning_message["name"] = (
                f"This name is currently listed as a synonym in ChEBI, which may suggest a duplication. "
                f"Please review {', '.join(compound_links)} to ensure there is no overlap, or contact the "
                f"ChEBI team if unsure."
            )
        return name


NamesFormSet = modelformset_factory(Names, form=NamesForm, extra=1, can_delete=True)


class DatabaseAccessionForm(forms.ModelForm):
    type = forms.ChoiceField(
        choices=DatabaseAccessionTypeChoices.choices,
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:validate_source", kwargs={"form_type": "database-accession"}),
                "hx-target": "next .database-accession-source-container",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
            }
        ),
    )
    source = forms.ModelChoiceField(
        queryset=Source.objects.filter(
            Q(is_manual_xref_source=True)
            | Q(is_registry_number_source=True)
            | Q(is_citation_source=True)
            | Q(is_cas_source=True)
        )
    )

    class Meta:
        model = DatabaseAccession
        fields = ["accession_number", "type", "source"]


DatabaseAccessionFormSet = modelformset_factory(DatabaseAccession, form=DatabaseAccessionForm, extra=1, can_delete=True)


class ChemicalDataForm(forms.ModelForm):
    mol_default_structure = forms.CharField(required=False, widget=forms.HiddenInput())

    class Meta:
        model = ChemicalData
        fields = ["formula", "charge", "mol_default_structure"]
        widgets = {
            "formula": forms.Textarea(
                attrs={
                    "rows": 2,
                    "hx-post": reverse_lazy("submitter:formula_to_mass"),
                    "hx-target": "#chemical-data-mass-container",
                    "hx-swap": "outerHTML",
                    "hx-indicator": "#htmx-loader",
                    "hx-trigger": "keyup changed delay:700ms",
                }
            ),
        }

    def _save_default_structure(self, molfile, compound, status, created_by, chemical_data):
        """
        Saves the default Structure information (always auto-generated from molfile)
        """
        return Structure.objects.create(
            molfile=molfile,
            compound=compound,
            status=status,
            created_by=created_by,
            default_structure=True,
            smiles=chemical_data.get("smiles"),
            standard_inchi=chemical_data.get("inchi"),
            standard_inchi_key=chemical_data.get("inchi_key"),
        )

    def save_with_structure(self, compound, status, created_by, molfile):
        def set_masses_from_formula(formula):
            """Helper function to calculate and set mass and monoisotopic mass from formula"""
            masses = mass_from_formula(formula)
            self.instance.mass = masses["mass"]
            self.instance.monoisotopic_mass = masses["monoisotopic_mass"]

        if not (molfile or self.instance.formula):
            # If there is no structure or formula provided, we don't save anything
            return None

        if molfile and not is_molfile_empty(molfile):
            # If molfile exists then create default structure using it
            # all structure properties are always auto generated based on molfile if it exists
            chemical_data = get_chemical_data_from_molfile(molfile)
            structure = self._save_default_structure(molfile, compound, status, created_by, chemical_data)
            self.instance.structure = structure

            # Mass is always auto generated based on structure if it exists
            self.instance.mass = chemical_data.get("mass")
            self.instance.monoisotopic_mass = chemical_data.get("monoisotopic_mass")

            if has_r_group(molfile):
                # For R-groups, submitter have the option to provide their own formula/charge which is already
                # part of chemical_data object as the form was submitted
                # Verify if this formula and charge are same as auto generated from molfile
                if (
                    chemical_data.get("formula") == self.instance.formula
                    and chemical_data.get("charge") == self.instance.charge
                ):
                    # Here, do not recalculate mass if charge was changed
                    # If same, leave everything as is. Formula and charge are already part of chemical_data object
                    self.instance.is_autogenerated = True
                else:
                    # If different, then recalculate masses based on formula.
                    # Submitter provided formula/charge are already part of chemical_data object
                    set_masses_from_formula(self.instance.formula)
                    self.instance.is_autogenerated = False
            else:
                # Formula and charge are always auto generated if it is not an R-group
                self.instance.formula = chemical_data.get("formula")
                self.instance.charge = chemical_data.get("charge")
                self.instance.is_autogenerated = True

        else:
            # If structure doesn't exist, then formula & charge provided by submitter are used
            # and mass is calculated based on formula
            set_masses_from_formula(self.instance.formula)
            self.instance.structure = None
            self.instance.is_autogenerated = False

        self.instance.compound = compound
        self.instance.status = status
        self.instance.created_by = created_by
        return super().save()


class CompoundOriginsForm(forms.ModelForm):
    species = forms.CharField(
        label="Species Name",
        required=False,
        widget=Select2SpeciesWidget(attrs={"class": "species-text-select form-control"}),
        help_text='<a href="#" onclick="hideSelect2Field(this, event, \'species\')">Add New Species</a>',
    )
    component = forms.CharField(
        required=False,
        widget=Select2ComponentWidget(attrs={"class": "component-text-select form-control"}),
        help_text='<a href="#" onclick="hideSelect2Field(this, event, \'component\')">Add New Component</a>',
    )
    species_source = forms.ModelChoiceField(queryset=Source.objects.filter(is_species_accession_source=True))
    source = forms.ModelChoiceField(
        queryset=Source.objects.filter(is_compound_origin_source=True),
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:get_source_accession_field"),
                "hx-target": "next .compound-origin-source-accession-container",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
            }
        ),
    )

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get("DELETE"):
            # Don't run validation on deleted formset
            if not any(
                cleaned_data.get(field) for field in ["species", "species_text", "species_source", "species_accession"]
            ):
                # Mark species field is required if species-select2 field & all manual fields are empty
                self.add_error("species", "This field is required")

            if not any(cleaned_data.get(field) for field in ["component", "component_text", "component_accession"]):
                # Mark component field is required if component-select2 field & all manual fields are empty
                self.add_error("component", "This field is required")

        return cleaned_data

    class Meta:
        model = CompoundOrigins
        fields = [
            "species",
            "species_text",
            "species_accession",
            "species_source",
            "component",
            "component_text",
            "component_accession",
            "strain_text",
            "strain_accession",
            "source",
            "source_accession",
            "comments",
        ]
        widgets = {
            "comments": forms.Textarea(attrs={"rows": 2, "required": False}),
        }


CompoundOriginsFormSet = modelformset_factory(CompoundOrigins, form=CompoundOriginsForm, extra=1, can_delete=True)
