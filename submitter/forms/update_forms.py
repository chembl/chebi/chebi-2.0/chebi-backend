from datetime import date, timedelta
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.template import loader
from django.urls import reverse_lazy

from chebi_models.models import Names, User, Source, Compound, DatabaseAccession
from chebi_models.models.choices import NameTypeSubmitterFormChoices, DatabaseAccessionTypeChoices
from submitter.utils import get_default_submitter_status
from utils import send_email


class DynamicSourceForm(forms.Form):
    source = forms.ModelChoiceField(queryset=Source.objects.none())

    def __init__(self, *args, source_type=None, htmx_source_field=False, source_accession_field_name=None, **kwargs):
        super().__init__(*args, **kwargs)
        form_mapping = {
            "SYNONYM": Q(is_synonym_source=True),
            "INN": Q(is_inn_source=True),
            "BRAND NAME": Q(is_brand_name_source=True),
            "IUPAC NAME": Q(is_iupac_source=True),
            "MANUAL_X_REF": Q(is_manual_xref_source=True),
            "REGISTRY_NUMBER": Q(is_registry_number_source=True),
            "CITATION": Q(is_citation_source=True),
            "CAS": Q(is_cas_source=True),
        }
        if source_type:
            self.fields["source"].queryset = Source.objects.filter(is_active=True).filter(
                form_mapping.get(source_type, Q())
            )
        if htmx_source_field:
            self.fields["source"].widget.attrs.update(
                {
                    "hx-get": reverse_lazy("submitter:get_source_accession_field"),
                    "hx-target": "next .mb-3",
                    "hx-swap": "innerHTML",
                    "hx-indicator": "#htmx-loader",
                }
            )
            if source_accession_field_name:
                self.fields["source"].widget.attrs.update(
                    {"hx-vals": f'{{"source_accession_field_name": "{source_accession_field_name}"}}'}
                )


class UpdateNamesForm(forms.ModelForm):
    # TODO: Source choices are not filtered based on type, submitter can possibly choose an invalid source
    source = forms.ModelChoiceField(
        queryset=Source.objects.filter(
            Q(is_synonym_source=True) | Q(is_inn_source=True) | Q(is_iupac_source=True) | Q(is_brand_name_source=True)
        )
    )
    type = forms.ChoiceField(
        choices=NameTypeSubmitterFormChoices.choices,
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:htmx_get_source"),
                "hx-target": "next .mb-3",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
            }
        ),
    )

    class Meta:
        model = Names
        fields = ["name", "type", "source", "language_code"]
        widgets = {
            "name": forms.Textarea(attrs={"rows": 2}),
        }

    def clean(self):
        cleaned_data = super().clean()
        if (
            Names.objects.filter(
                compound_id__in=self.instance.compound.compound_with_children_ids,
                name__iexact=cleaned_data["name"],
                type__iexact=cleaned_data["type"],
                source_id=cleaned_data["source"],
            )
            .exclude(pk=self.instance.pk)
            .exists()
        ):
            raise ValidationError("Same synonym already exists for this entry.")

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.ascii_name = self.cleaned_data.get("name")
        if commit:
            instance.save()
        return instance


class SubmitterCreateNamesForm(forms.ModelForm):
    source = forms.ModelChoiceField(
        queryset=Source.objects.filter(
            Q(is_synonym_source=True) | Q(is_inn_source=True) | Q(is_iupac_source=True) | Q(is_brand_name_source=True)
        )
    )
    type = forms.ChoiceField(
        choices=NameTypeSubmitterFormChoices.choices,
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:htmx_get_source"),
                "hx-target": "next .mb-3",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
            }
        ),
    )

    class Meta:
        model = Names
        fields = ["name", "type", "source", "language_code"]
        widgets = {
            "name": forms.Textarea(attrs={"rows": 2}),
        }

    def __init__(self, *args, **kwargs):
        self.compound_id = kwargs.pop("compound_id", None)
        self.created_by_id = kwargs.pop("created_by_id", None)
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        compound = Compound.objects.get(pk=self.compound_id)
        if Names.objects.filter(
            compound_id__in=compound.compound_with_children_ids,
            name__iexact=cleaned_data["name"],
            type__iexact=cleaned_data["type"],
            source_id=cleaned_data["source"],
        ).exists():
            raise ValidationError("Same synonym already exists for this entry.")

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.ascii_name = self.cleaned_data.get("name")
        instance.compound_id = self.compound_id
        instance.created_by_id = self.created_by_id
        instance.status = get_default_submitter_status()
        if commit:
            instance.save()
        if instance.compound.stars == 3:
            # If it is a 3-star entry, notify curators that a new synonym has been added
            send_email(
                subject=f"ChEBI:{self.compound_id} - New Synonym Added",
                body=loader.render_to_string("submitter/emails/synonym-added.html", {"instance": instance}),
                to=settings.CONTACT_EMAIL_TO,
            )
        return instance


class SubmitterCreateXRefForm(forms.ModelForm):
    source = forms.ModelChoiceField(
        queryset=Source.objects.filter(
            Q(is_manual_xref_source=True)
            | Q(is_registry_number_source=True)
            | Q(is_citation_source=True)
            | Q(is_cas_source=True)
        )
    )
    type = forms.ChoiceField(
        choices=DatabaseAccessionTypeChoices.choices,
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:htmx_get_source"),
                "hx-target": "next .mb-3",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
                "hx-vals": '{"htmx_source_field": "true", "source_accession_field_name": "accession_number"}',
            }
        ),
    )

    class Meta:
        model = DatabaseAccession
        fields = ["type", "source", "accession_number"]

    def __init__(self, *args, **kwargs):
        self.compound_id = kwargs.pop("compound_id", None)
        self.created_by_id = kwargs.pop("created_by_id", None)
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        compound = Compound.objects.get(pk=self.compound_id)
        if DatabaseAccession.objects.filter(
            compound_id__in=compound.compound_with_children_ids,
            accession_number__iexact=cleaned_data["accession_number"],
            type__iexact=cleaned_data["type"],
            source_id=cleaned_data["source"],
        ).exists():
            raise ValidationError("Same cross reference already exists for this entry.")

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.compound_id = self.compound_id
        instance.created_by_id = self.created_by_id
        instance.status = get_default_submitter_status()
        if commit:
            instance.save()
        if instance.compound.stars == 3:
            # If it is a 3-star entry, notify curators that a new XRef has been added
            send_email(
                subject=f"ChEBI:{self.compound_id} - New Cross Reference Added",
                body=loader.render_to_string("submitter/emails/xref-added.html", {"instance": instance}),
                to=settings.CONTACT_EMAIL_TO,
            )
        return instance


class SubmitterUpdateXRefForm(forms.ModelForm):
    # TODO: Source choices are not filtered based on type, submitter can possibly choose an invalid source
    source = forms.ModelChoiceField(
        queryset=Source.objects.filter(
            Q(is_manual_xref_source=True)
            | Q(is_registry_number_source=True)
            | Q(is_citation_source=True)
            | Q(is_cas_source=True)
        ),
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:get_source_accession_field"),
                "hx-target": "next .mb-3",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
                "hx-vals": '{"source_accession_field_name": "accession_number"}',
            }
        ),
    )
    type = forms.ChoiceField(
        choices=DatabaseAccessionTypeChoices.choices,
        widget=forms.Select(
            attrs={
                "hx-get": reverse_lazy("submitter:htmx_get_source"),
                "hx-target": "next .mb-3",
                "hx-swap": "innerHTML",
                "hx-indicator": "#htmx-loader",
                "hx-vals": '{"htmx_source_field": "true", "source_accession_field_name": "accession_number"}',
            }
        ),
    )

    class Meta:
        model = DatabaseAccession
        fields = ["type", "source", "accession_number"]

    def clean(self):
        cleaned_data = super().clean()
        compound = Compound.objects.get(pk=self.instance.compound_id)
        if DatabaseAccession.objects.filter(
            compound_id__in=compound.compound_with_children_ids,
            accession_number__iexact=cleaned_data["accession_number"],
            type__iexact=cleaned_data["type"],
            source_id=cleaned_data["source"],
        ).exists():
            raise ValidationError("Same cross reference already exists for this entry.")


class SubmitterProfileForm(forms.ModelForm):
    email = forms.EmailField(disabled=True, help_text="Please contact us if you need to change your email address")

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email", "address", "affiliation", "public_name", "is_anonymous"]
        widgets = {"address": forms.Textarea(attrs={"rows": 2})}


class UpdateCompoundForm(forms.ModelForm):
    class Meta:
        model = Compound
        fields = ["definition", "submitter_notes", "release_date"]
        widgets = {
            "definition": forms.Textarea(attrs={"rows": 2}),
            "submitter_notes": forms.Textarea(attrs={"rows": 2}),
            "release_date": forms.DateInput(attrs={"min": date.today() + timedelta(days=1), "type": "date"}),
        }
        help_texts = {"release_date": "Date when your entry should be released publicly."}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # If the entry is already released, do not show release date field
        if self.instance.is_released:
            self.fields.pop("release_date")

    def clean(self):
        cleaned_data = super().clean()
        release_date = cleaned_data.get("release_date")
        if self.instance.is_released:
            cleaned_data.pop("release_date", None)
        else:
            if release_date and date.today() > release_date:
                raise ValidationError(
                    "Please provide a future release date, or leave this field blank for your "
                    "submission to be released publicly now."
                )
        return cleaned_data
