from django import forms

from chebi_models.models import Source


class Select2SpeciesWidget(forms.TextInput):
    def format_value(self, value):
        if value and isinstance(value, str):
            text, accession, source_id = value.split("@")
            source = Source.objects.filter(pk=source_id).first()
            prefix = getattr(source, "prefix", None)
            text = f"{text} ({prefix}:{accession})" if prefix else f"{text} ({accession})"
            return f"{text}${value}"
        return value


class Select2ComponentWidget(forms.TextInput):
    def format_value(self, value):
        if value and isinstance(value, str):
            text, accession = value.split("@")
            text = f"{text} ({accession})"
            return f"{text}${value}"
        return value
