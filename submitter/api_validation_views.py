"""
Contains all API Validation Rules used in Submitter Tool
"""

import re

from django.core.exceptions import ValidationError
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template import Template, Context
from libRDChEBI.descriptors import has_r_group
from rest_framework.views import APIView

from chebi_models.models import Source, CompoundOrigins
from submitter.forms.submit_forms import SubmitCompoundForm, NamesForm, DatabaseAccessionForm
from submitter.utils import is_molfile_empty, get_chemical_data_from_molfile, mass_from_formula


class ValidateChebiName(APIView):
    """
    ChEBI Name is the name of the compound. It must be unique, show an error in case of duplication.
    Show a warning if same name exists as synonym, but don't restrict submission in this case.
    """

    def get(self, request):
        form = SubmitCompoundForm(request.GET)
        try:
            form.full_clean()
        except ValidationError:
            # No need to add the error to the form again
            # The error has already been added by clean method
            pass
        return HttpResponse(Template("{{ form.name.as_field_group }}").render(Context({"form": form})))


class ValidateSynonym(APIView):
    """
    Validate the Synonym Name for existing similar names
    Show warning if same name exists as a synonym in another compound
    """

    def get(self, request):
        name_key = list(request.GET.keys())[0]
        name_value = list(request.GET.values())[0]
        form = NamesForm({"name": name_value})
        form.fields["name"].widget.attrs.update(
            {
                "name": name_key,
                "id": f"id_{name_key}",
            }
        )
        try:
            form.full_clean()
        except ValidationError:
            # We don't expect any validation error to occur when cleaning the form but even if
            # there's any we can ignore it as the error is already added to the field errors
            pass
        return HttpResponse(Template("{{ form.name.as_field_group }}").render(Context({"form": form})))


class HtmxSubmitCompoundGetSources(APIView):
    """
    A generic View that takes a source type and returns list of available sources for that source type
    Maintains a mapping of each source type to a list of source names
    This view is generalised to work for Names and Database Accessions
    """

    form_mapping = {
        "name": {
            "form_class": NamesForm,
            "filters": {
                "SYNONYM": Q(is_synonym_source=True),
                "INN": Q(is_inn_source=True),
                "BRAND NAME": Q(is_brand_name_source=True),
                "IUPAC NAME": Q(is_iupac_source=True),
            },
            "prefix": "names",
        },
        "database-accession": {
            "form_class": DatabaseAccessionForm,
            "filters": {
                "MANUAL_X_REF": Q(is_manual_xref_source=True),
                "REGISTRY_NUMBER": Q(is_registry_number_source=True),
                "CITATION": Q(is_citation_source=True),
                "CAS": Q(is_cas_source=True),
            },
            "prefix": "database-accession",
        },
    }

    def get(self, request, form_type):
        if form_type not in self.form_mapping:
            return HttpResponse(status=400)

        key = list(request.GET.keys())[0]
        form_id = re.findall(r"\d+", key)[0]
        source_type = list(request.GET.values())[0]

        form_config = self.form_mapping[form_type]
        form = form_config["form_class"]()
        qs = Source.objects.filter(is_active=True)
        form.fields["source"].queryset = qs.filter(form_config["filters"].get(source_type, Q()))

        # Update the name and id attributes dynamically
        form.fields["source"].widget.attrs.update(
            {
                "name": f"{form_config['prefix']}-{form_id}-source",
                "id": f"id_{form_config['prefix']}-{form_id}-source",
            }
        )
        return HttpResponse(Template("{{ form.source.as_field_group }}").render(Context({"form": form})))


class GetChemicalDataFromMol(APIView):
    def post(self, request):
        mol = request.data.get("mol")
        if is_molfile_empty(mol):
            # If molfile is empty, we don't need to calculate any properties
            # Allow user to provide their own formula and mass would be calculated based on it
            chemical_data = {
                "can_edit_chemical_data": True,
                "mol_default_structure": mol,
            }
        else:
            chemical_data = get_chemical_data_from_molfile(mol)
            # Allow submitter to edit formula or charge only if it's an R-group
            chemical_data["can_edit_chemical_data"] = has_r_group(mol)
            chemical_data["mol_default_structure"] = mol
        return render(request, template_name="submitter/partials/chemical_data-container.html", context=chemical_data)


class GetMassFromFormula(APIView):
    def post(self, request):
        return render(
            request,
            template_name="submitter/partials/chemical-data-mass-container.html",
            context=mass_from_formula(request.data.get("formula")),
        )


class SpeciesSearch(APIView):
    def get(self, request):
        query = request.GET.get("q", "")
        species = CompoundOrigins.objects.filter(species_text__icontains=query).distinct(
            "species_text", "species_accession", "species_source"
        )[:50]

        results = [
            {
                # Encode ID with form field data needed at FE
                "id": f"{sp.species_text}@{sp.species_accession}@{sp.species_source_id}",
                "text": f'{sp.species_text} ({getattr(sp.species_source, "prefix", "")}:{sp.species_accession})',
            }
            for sp in species
        ]
        return JsonResponse({"results": results})


class ComponentSearch(APIView):
    def get(self, request):
        query = request.GET.get("q", "")
        qs = CompoundOrigins.objects.filter(component_text__icontains=query).distinct(
            "component_text", "component_accession"
        )[:50]

        results = [
            {
                "id": f"{cp.component_text}@{cp.component_accession}",
                "text": f"{cp.component_text} ({cp.component_accession})",
            }
            for cp in qs
        ]
        return JsonResponse({"results": results})


class GetSourceAccessionField(APIView):
    """
    Generic HTMX View used by Inline formsets and regular forms as well
    Expects an optional field_name for source_accession along with source ID
    Renders the source accession field along with prefix and with specified name if provided
    """

    def get(self, request, *args, **kwargs):
        params = dict(request.GET)
        source_accession_field_name = "source_accession"
        if params.get("source_accession_field_name"):
            source_accession_field_name = params.get("source_accession_field_name")[0]
            params.pop("source_accession_field_name")

        key = next(iter(params), "")
        value = params.get(key)[0]
        if key.endswith("-source"):
            # Extract prefix if this request came from inline formset
            prefix = key.rsplit("-", 1)[0]
        else:
            prefix = ""
        source = Source.objects.filter(pk=value).first()
        return render(
            request,
            template_name="submitter/partials/source_accession_field.html",
            context={"source": source, "prefix": prefix, "source_accession_field_name": source_accession_field_name},
        )
