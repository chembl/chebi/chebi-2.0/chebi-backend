"""
Contains all project-wide constants
"""

# The top level role class, that is parent of all ontology classifications
CHEBI_ROLE_ID = 50906
