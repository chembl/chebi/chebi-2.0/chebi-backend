import os
import logging

from django.core.mail import mail_admins
from elasticsearch_dsl import Search, connections
from elasticsearch_dsl.index import Index

logger = logging.getLogger(__name__)


"""
This is the central configuration file for Elastic Search
All connection configs to be defined here and ES_SEARCH from this file is to be used
    - Creates an ES connection with provided configs
    - Using default timeout of 10s
    - Import ES_SEARCH wherever we need to use ES
"""


def create_es_connection():
    es_host = os.environ.get("ES_HOST")
    es_port = os.environ.get("ES_PORT")

    if not es_host or not es_port:
        logger.warning("Elasticsearch settings missing. ES will be disabled.")
        return None

    try:
        es_connection = connections.create_connection(
            hosts=f"{es_host}:{es_port}",
            http_auth=(os.environ.get("ES_USER"), os.environ.get("ES_PASSWORD")),
        )
        es_ping = es_connection.ping()
        if not es_ping:
            logger.exception("Elasticsearch is unreachable. Disabling ES connection.")
            mail_admins(subject="ElasticSearch is unreachable.", message=f"ElasticSearch is unreachable {es_ping}")
            return None
        return es_connection

    except Exception as e:
        logger.exception(f"Elasticsearch settings error: {e}")
        mail_admins(subject="Elasticsearch settings error.", message=f"Elasticsearch settings error {e}")
        return None


# TODO: Rewrite usage of following ES references by handling the case where ES does not exist

ES_CONNECTION = create_es_connection()

ES_SEARCH = Search(using=ES_CONNECTION, index=os.environ.get("ES_ALIAS")) if ES_CONNECTION else None

ES_INDEX = Index(name=os.environ.get("ES_ALIAS"), using=ES_CONNECTION) if ES_CONNECTION else None

MAPPINGS = ES_INDEX.get_mapping() if ES_INDEX and ES_CONNECTION else None
