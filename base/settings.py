"""
Django settings for base project.

Generated by 'django-admin startproject' using Django 4.1.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.1/ref/settings/
"""

import os
from pathlib import Path

from django.contrib import messages
from dotenv import load_dotenv

load_dotenv()
# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.1/howto/deployment/checklist/

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")

DEBUG = os.environ.get("DEBUG", False)

ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "127.0.0.1").split(",")

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.forms",
    "chebi_models",
    "rest_framework",
    "drf_spectacular",
    "drf_spectacular_sidecar",
    "corsheaders",
    "authentication",
    "submitter",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.AllowAllUsersModelBackend"]

MESSAGE_TAGS = {
    # Updated Error tag to match with bootstrap alert classes
    messages.ERROR: "danger",
}


AUTH_USER_MODEL = "chebi_models.User"

ROOT_URLCONF = "base.urls"

# BASE_URL is the subdirectory URL where Django app is served
# Only a few selected url patterns are passed over to the Django app, rest are handled by frontend
BASE_URL = os.environ.get("BASE_URL", "chebi/alpha/")

# Generate absolute URL based on BASE_URL for redirection to the frontend app
FRONTEND_APP_URL = "/" + BASE_URL if BASE_URL else "/"
LOGIN_URL = FRONTEND_APP_URL + "auth/login"
LOGOUT_REDIRECT_URL = FRONTEND_APP_URL
LOGIN_REDIRECT_URL = FRONTEND_APP_URL + "submit/"
MARVIN_JS_BASE_URL = os.environ.get("MARVIN_JS_BASE_URL", "")

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "base.template_context.extra_context",
            ],
        },
    },
]

FORM_RENDERER = "django.forms.renderers.TemplatesSetting"

WSGI_APPLICATION = "base.wsgi.application"

# Database
# https://docs.djangoproject.com/en/4.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.environ.get("DB_NAME"),
        "USER": os.environ.get("DB_USER"),
        "PASSWORD": os.environ.get("DB_PASSWORD"),
        "HOST": os.environ.get("DB_HOST"),
        "PORT": os.environ.get("DB_PORT", "5432"),
        "OPTIONS": {"options": "-c search_path=" + os.environ.get("DB_SCHEMA", "public")},
    }
}

# Password validation
# https://docs.djangoproject.com/en/4.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/4.1/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = False

USE_TZ = True

REST_FRAMEWORK = {
    # TODO: set django models permissions accordingly
    # "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly"],
    "DEFAULT_RENDERER_CLASSES": [
        "rest_framework.renderers.JSONRenderer",
        "rest_framework.renderers.BrowsableAPIRenderer",
    ],
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "DEFAULT_PAGINATION_CLASS": "base.pagination.ChebiPagination",
}

SPECTACULAR_SETTINGS = {
    "TITLE": "ChEBI 2.0",
    "DESCRIPTION": "ChEBI 2.0 WebServices",
    "VERSION": "2.0.0",
    "SERVE_INCLUDE_SCHEMA": False,
    "SWAGGER_UI_DIST": "SIDECAR",
    "SWAGGER_UI_FAVICON_HREF": "SIDECAR",
    "SORT_OPERATION_PARAMETERS": False,
    "REDOC_DIST": "SIDECAR",
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.1/howto/static-files/

STATIC_URL = BASE_URL + "static/"

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
STATIC_ROOT = os.path.join(BASE_DIR, "static")
MEDIA_URL = BASE_URL + "media/"

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "templates", "assets"),
]
# Remove when upgrade Django to 4.2+ use below commented code
# TODO: Temp patch, RUn without staticfiles storage
# STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

# STORAGES = {
#     "staticfiles": {
#         "BACKEND": "whitenoise.storage.CompressedManifestStaticFilesStorage",
#     },
# }

# Default primary key field type
# https://docs.djangoproject.com/en/4.1/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

CORS_ALLOW_ALL_ORIGINS = True
# TODO: Temporary hack
CSRF_TRUSTED_ORIGINS = ["https://wwwdev.ebi.ac.uk"]

if DEBUG:
    INSTALLED_APPS += ["debug_toolbar"]
    MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]
    INTERNAL_IPS = [os.environ.get("DEBUG_TOOLBAR_INTERNAL_IP")]

# Email Configuration
EMAIL_BACKEND = (
    "django.core.mail.backends.console.EmailBackend"
    if os.environ.get("CONSOLE_EMAIL")
    else "django.core.mail.backends.smtp.EmailBackend"
)
EMAIL_HOST = os.environ.get("EMAIL_HOST")
DEFAULT_FROM_EMAIL = os.environ.get("DEFAULT_FROM_EMAIL")
# Error Email Configuration
SERVER_EMAIL = DEFAULT_FROM_EMAIL
ADMINS = [tuple(admin.split(";")) for admin in os.getenv("EMAIL_ADMINS", "").split(",") if admin]
MANAGERS = ADMINS
EMAIL_SUBJECT_PREFIX = os.environ.get("EMAIL_SUBJECT_PREFIX")

# Config for contact for email
CONTACT_EMAIL_TO = os.getenv("CONTACT_EMAIL_TO")
