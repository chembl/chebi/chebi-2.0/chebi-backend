"""
Additional Custom context passed to the Django templates
"""

from django.conf import settings


def extra_context(request):
    """
    Returns FRONTEND_APP_URL in template context
    """
    return {
        "FRONTEND_APP_URL": settings.FRONTEND_APP_URL,
        "MARVIN_JS_BASE_URL": settings.MARVIN_JS_BASE_URL,
    }
