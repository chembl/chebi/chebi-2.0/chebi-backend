"""
Contains project-wide common utilities
"""

from django.core.mail import EmailMessage, mail_admins


def capture_manual_exception(
    message: str,
    subject: str = "Manual Exception ",
):
    """
    Helper method to capture manually raised exceptions and pass it to admins
    Used in codebase to monitor areas that could potentially raise errors
    """
    mail_admins(subject=subject, message=message, fail_silently=False)


def send_email(subject, body, to, reply_to=None, from_email=None):
    """
    Helper for sending emails, to be used site-wide.
    Expects a 'subject' string, 'to' as a string or list of emails, and 'message'
    """
    # TODO: Move this to Celery, save email messages log to DB
    email = EmailMessage(
        subject=subject, body=body, to=[to] if isinstance(to, str) else to, reply_to=reply_to, from_email=from_email
    )
    email.send()


def standardise_chebi_id(chebi_id_str: str) -> str:
    """
    Standardises the chebi id, examples:
    17 -> CHEBI:17
    chebi:69 -> CHEBI:69
    ChEBI:123 -> CHEBI:123
    CHEBI:27732 -> CHEBI:27732
    """
    chebi_id_str = chebi_id_str.strip()
    chebi_id = None
    if chebi_id_str.lower().startswith("chebi:"):
        if len(chebi_id_str) > len("chebi:"):
            chebi_id = chebi_id_str.split(":")[1]
    else:
        chebi_id = chebi_id_str

    try:
        chebi_id = int(chebi_id.strip())
    except ValueError:
        raise ValueError(f'"{chebi_id_str}" is not a valid ChEBI ID')

    return f"CHEBI:{chebi_id}"


def str_to_bool(value: str | None) -> bool | None:
    """
    Parses a string value into the corresponding boolean value
    """
    if isinstance(value, str):
        if value.lower() in ["true", "1", "t", "y", "yes"]:
            return True
        elif value.lower() in ["false", "0", "f", "n", "no"]:
            return False
    return None
