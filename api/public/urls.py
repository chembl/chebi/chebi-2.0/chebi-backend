from django.urls import path, re_path

from api.public.views.compound_detail import (
    AdvancedSearchFormDataAPI,
    CompoundDetailAPI,
    CompoundGetOntologyParentsAPI,
    CompoundGetOntologyChildrenAPI,
    CompoundsListAPI,
    ContactFormView,
    MolFileDownloadView,
    StructureSvgView,
)
from api.public.views.elastic_search import AdvancedSearchAPI, ElasticSearchAPI, StructureSearchAPI, OntologySearchAPI
from api.public.views.structure_calculations import (
    MolFormulaAPI,
    NetChargeAPI,
    AvgMassAPI,
    MonoisotopicMassAPI,
    AvgMassFromFormulaAPI,
    MonoisotopicMassFromFormulaAPI,
    DepictIndigoAPI,
)

app_name = "public_api"

urlpatterns = [
    re_path(r"compound/(?P<chebi_id>([cC][hH][eE][bB][iI]:)?\d+)/", CompoundDetailAPI.as_view()),
    path("compounds/", CompoundsListAPI.as_view()),
    path("structure_search/", StructureSearchAPI.as_view()),
    path("ontology/all_children_in_path/", OntologySearchAPI.as_view()),
    re_path(r"ontology/children/(?P<chebi_id>([cC][hH][eE][bB][iI]:)?\d+)/", CompoundGetOntologyChildrenAPI.as_view()),
    re_path(r"ontology/parents/(?P<chebi_id>([cC][hH][eE][bB][iI]:)?\d+)/", CompoundGetOntologyParentsAPI.as_view()),
    path("es_search/", ElasticSearchAPI.as_view()),
    path("advanced_search/", AdvancedSearchAPI.as_view()),
    path("advanced_search/form_data", AdvancedSearchFormDataAPI.as_view()),
    path("structure/<pk>/", StructureSvgView.as_view()),
    path("structure-calculations/depict-indigo/", DepictIndigoAPI.as_view()),
    path("structure-calculations/mol-formula/", MolFormulaAPI.as_view()),
    path("structure-calculations/net-charge/", NetChargeAPI.as_view()),
    path("structure-calculations/avg-mass/", AvgMassAPI.as_view()),
    path("structure-calculations/monoisotopic-mass/", MonoisotopicMassAPI.as_view()),
    path("structure-calculations/avg-mass/from-formula/", AvgMassFromFormulaAPI.as_view()),
    path("structure-calculations/monoisotopic-mass/from-formula/", MonoisotopicMassFromFormulaAPI.as_view()),
    path("molfile/<pk>/", MolFileDownloadView.as_view(), name="download_molfile"),
    path("contact/", ContactFormView.as_view()),
]
