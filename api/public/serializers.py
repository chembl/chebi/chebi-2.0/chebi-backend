from collections import defaultdict

from rest_framework import serializers

from api.utils.es_responses import SimpleResponse
from base.es_settings import ES_SEARCH
from chebi_models.models import (
    ChemicalData,
    Compound,
    CompoundOrigins,
    DatabaseAccession,
    Names,
    Relation,
    Structure,
)


class NameSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    ascii_name = serializers.CharField()
    source = serializers.CharField(source="source.name")
    status = serializers.CharField(source="status.code")

    class Meta:
        model = Names
        fields = ["name", "status", "type", "source", "ascii_name", "adapted", "language_code"]


class StructureSerializer(serializers.ModelSerializer):
    wurcs = serializers.SerializerMethodField()

    def get_wurcs(self, obj):
        return getattr(obj, "wurcs", None) and obj.wurcs.wurcs

    class Meta:
        model = Structure
        fields = ["id", "smiles", "standard_inchi", "standard_inchi_key", "wurcs", "is_r_group"]


class CompoundOriginSerializer(serializers.ModelSerializer):
    source = serializers.CharField(source="source.name")
    source_accession_url = serializers.SerializerMethodField()
    species_accession_prefix = serializers.SerializerMethodField()

    def get_species_accession_prefix(self, obj):
        # TODO: Source should always be present, remove this condition after chebi-etl/-/issues/78
        return obj.species_source.prefix if obj.species_source else None

    def get_source_accession_url(self, obj):
        if obj.source.url:
            return obj.source.url.replace("*", obj.source_accession)

    class Meta:
        model = CompoundOrigins
        fields = [
            "species_text",
            "species_accession",
            "component_text",
            "component_accession",
            "strain_text",
            "strain_accession",
            "source",
            "source_accession",
            "comments",
            "species_accession_url",
            "component_accession_url",
            "source_accession_url",
            "species_accession_prefix",
        ]


class ChemicalDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChemicalData
        fields = ["formula", "charge", "mass", "monoisotopic_mass"]


class RelationSerializer(serializers.ModelSerializer):
    relation_type = serializers.CharField(source="relation_type.description")
    init_name = serializers.SerializerMethodField()
    final_name = serializers.SerializerMethodField()
    init_id = serializers.SerializerMethodField()
    final_id = serializers.SerializerMethodField()

    # TODO: When merging an entry, make sure there isn't same relation in parent and child
    #  Otherwise It will make the relation appear twice

    def get_init_name(self, obj):
        if obj.init.parent:
            return obj.init.parent.name
        return obj.init.name

    def get_final_name(self, obj):
        if obj.final.parent:
            return obj.final.parent.name
        return obj.final.name

    def get_init_id(self, obj):
        if obj.init.parent:
            return obj.init.parent_id
        return obj.init_id

    def get_final_id(self, obj):
        if obj.final.parent:
            return obj.final.parent_id
        return obj.final_id

    class Meta:
        model = Relation
        fields = ["init_id", "init_name", "relation_type", "final_id", "final_name"]


class DatabaseAccessionSerializer(serializers.ModelSerializer):
    source_name = serializers.CharField(source="source.name")
    prefix = serializers.CharField(source="source.prefix")
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        if obj.type == "CAS" and not obj.source.has_cas_url:
            # For CAS numbers, only show URL if that source supports CAS lookup via URL which is controlled via has_cas_url
            return None
        return obj.source.url.replace("*", obj.accession_number) if obj.source.url else None

    class Meta:
        model = DatabaseAccession
        fields = ["id", "accession_number", "type", "source_name", "url", "prefix"]


class CompoundDetailSerializer(serializers.ModelSerializer):
    chemical_data = ChemicalDataSerializer()
    names = serializers.SerializerMethodField()
    default_structure = StructureSerializer()
    name = serializers.CharField()
    ascii_name = serializers.CharField()
    definition = serializers.CharField()
    compound_origins = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    ontology_relations = serializers.SerializerMethodField()
    database_accessions = serializers.SerializerMethodField()
    roles_classification = serializers.SerializerMethodField()

    def __init__(self, instance=None, only_ontology_parents=False, only_ontology_children=False):
        if instance:
            super().__init__(instance)
        else:
            super().__init__()
        self.only_ontology_parents = only_ontology_parents
        self.only_ontology_children = only_ontology_children

    def get_roles_classification(self, obj):
        compound_roles = (
            ES_SEARCH.query("ids", values=[obj.pk]).extra(_source=["has_role"]).response_class(SimpleResponse)
        )
        results = compound_roles.execute()["hits"]["hits"]
        return results[0]["_source"].get("has_role") if results else None

    def get_database_accessions(self, obj):
        qs = (
            DatabaseAccession.public_objects_qs(stars=obj.stars)
            .filter(compound_id__in=obj.compound_with_children_ids)
            .select_related("source")
            .order_by("pk")
        )
        database_accessions_by_type = defaultdict(list)
        for accession in qs:
            database_accessions_by_type[accession.type].append(DatabaseAccessionSerializer(accession).data)
        return database_accessions_by_type

    def get_ontology_relations(self, obj):
        # chebi-frontend/-/issues/18
        relations_qs = Relation.objects.filter(status__code__in=["S", "C", "E"]).select_related(
            "relation_type", "init", "final"
        )
        ontology_data = {}
        if (self.only_ontology_parents == self.only_ontology_children) or self.only_ontology_children:
            ontology_data["incoming_relations"] = RelationSerializer(
                instance=relations_qs.filter(final_id__in=obj.compound_with_children_ids).order_by("relation_type"),
                many=True,
            ).data
        if (self.only_ontology_parents == self.only_ontology_children) or self.only_ontology_parents:
            ontology_data["outgoing_relations"] = RelationSerializer(
                instance=relations_qs.filter(init_id__in=obj.compound_with_children_ids).order_by("relation_type"),
                many=True,
            ).data
        return ontology_data

    def get_created_by(self, obj):
        return (
            Compound.objects.filter(pk__in=obj.compound_with_children_ids, created_by__is_anonymous=False)
            .values_list("created_by__public_name", flat=True)
            .distinct()
        )

    def get_names(self, obj):
        qs = (
            Names.public_objects_qs(stars=obj.stars)
            .filter(compound_id__in=obj.compound_with_children_ids)
            .select_related("status", "source")
            .order_by("pk")
        )
        names_by_type = defaultdict(list)
        for name in qs:
            names_by_type[name.type].append(NameSerializer(name).data)
        return names_by_type

    def get_compound_origins(self, obj):
        qs = (
            CompoundOrigins.public_objects_qs(stars=obj.stars)
            .filter(compound_id__in=obj.compound_with_children_ids)
            .order_by("pk")
            .select_related("status", "source")
        )
        return CompoundOriginSerializer(instance=qs, many=True).data

    class Meta:
        model = Compound
        fields = [
            "id",
            "chebi_accession",
            "name",
            "stars",
            "definition",
            "ascii_name",
            "names",
            "chemical_data",
            "modified_on",
            "secondary_ids",
            "default_structure",
            "compound_origins",
            "created_by",
            "ontology_relations",
            "database_accessions",
            "roles_classification",
            "is_released",
        ]


class CompoundOntologyOnlySerializer(CompoundDetailSerializer):
    ontology_relations = serializers.SerializerMethodField()

    def __init__(self, instance, only_ontology_parents=False, only_ontology_children=False):
        super().__init__(instance, only_ontology_parents, only_ontology_children)

    class Meta:
        model = Compound
        fields = [
            "id",
            "chebi_accession",
            "ontology_relations",
        ]


class PendingSubmissionCompoundSerializer(serializers.ModelSerializer):
    """
    Serializer used for Compounds that are scheduled to be released at some point in future
    Does not reveal any information about the compound.
    """

    class Meta:
        model = Compound
        fields = ["id", "chebi_accession", "is_released"]


class ContactFormSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    message = serializers.CharField(required=True)


class CompoundsFormSerializer(serializers.Serializer):
    chebi_ids = serializers.ListField(child=serializers.CharField(max_length=100))
