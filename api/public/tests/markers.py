import elasticsearch.exceptions
import pytest

from base.es_settings import ES_CONNECTION, ES_INDEX


def _require_es_connection():
    """This is a marker which skip a unit test to be executed if there is not a valid Elasticsearch connection set in
    the environment. For example: When you are running the test test_indexation_process locally and the VPN is off.
    Also, it is needed an index called chebi-subset-compounds with data properly mapped from chebi_for_testing.owl
    (https://ftp.ebi.ac.uk/pub/databases/chebi-2/unit_testing/)"""
    try:
        is_valid_connection = ES_CONNECTION.info() and ES_INDEX._name == "chebi-subset-compounds"
    except elasticsearch.exceptions.ConnectionError:
        is_valid_connection = False

    return pytest.mark.skipif(
        not is_valid_connection, reason="This unit tests requires a valid Elasticsearch connection"
    )


require_es_connection = _require_es_connection()
