import pytest
from rest_framework.test import RequestsClient


@pytest.fixture(scope="module")
def max_results_for_testing():
    return 2_000


@pytest.fixture(scope="module")
def test_client():
    return RequestsClient()
