from dataclasses import dataclass, field

from pydantic_models.advanced_search import Term


@dataclass
class GeneralSearchData:
    name: str | None
    search_term: Term | None
    expected_results: list[str | int] = field(default_factory=list)
    expected_total: int = 0

    def __post_init__(self):
        self.name = f"general_search_{self.name}"
        self.expected_total = len(self.expected_results)


general_search_test_cases = [
    GeneralSearchData(search_term=Term(term="10072"), name="chebi_id", expected_results=["CHEBI:10072"]),
    GeneralSearchData(search_term=Term(term="CHEBI:10072"), name="chebi_id", expected_results=["CHEBI:10072"]),
    GeneralSearchData(search_term=Term(term="CHEBI10072"), name="chebi_id", expected_results=["CHEBI:10072"]),
    GeneralSearchData(search_term=Term(term="CheBi:10072"), name="chebi_id", expected_results=["CHEBI:10072"]),
    GeneralSearchData(
        search_term=Term(term="FBZONXHGGPHHIY"),
        name="inchikey_first_part",
        expected_results=["CHEBI:10072", "CHEBI:71201"],
    ),
    GeneralSearchData(
        search_term=Term(term="FBZONXHGGPHHIY-UHFFFAOYSA"),
        name="inchikey_first_and_second_parts",
        expected_results=["CHEBI:10072", "CHEBI:71201"],
    ),
    GeneralSearchData(
        search_term=Term(term="O=C(O)c1cc(O)c2cccc(O)c2n1"), name="smiles", expected_results=["CHEBI:10072"]
    ),
    GeneralSearchData(
        search_term=Term(term="InChI=1S/CH3NO2/c2-1(3)4/h2H2,(H,3,4)/p-1"),
        name="inchi",
        expected_results=["CHEBI:13941"],
    ),
    GeneralSearchData(
        search_term=Term(term="3-(3,4-dihydroxyphenyl)-1-(2,4,6-trihydroxyphenyl)prop-2-en-1-one"),
        name="iupac_name",
        expected_results=["CHEBI:10836", "CHEBI:48967"],
    ),
    GeneralSearchData(search_term=Term(term="PruNeTol"), name="brand_name", expected_results=["CHEBI:28088"]),
    GeneralSearchData(
        search_term=Term(term="7α-hydroxyestradiol"), name="special_chebi_name", expected_results=["CHEBI:87598"]
    ),
    GeneralSearchData(
        search_term=Term(term="7alpha-hydroxyestradiol"), name="ascii_chebi_name", expected_results=["CHEBI:87598"]
    ),
    GeneralSearchData(
        search_term=Term(term="HMDB0000881"),
        name="cross_references_human_metabolite_database",
        expected_results=["CHEBI:10072", "CHEBI:71201"],
    ),
    GeneralSearchData(
        search_term=Term(term="4614128"), name="cross_references_pubmed_id", expected_results=["CHEBI:10072"]
    ),
    GeneralSearchData(
        search_term=Term(term="73692-51-0"),
        name="cross_references_cas_registry_number",
        expected_results=["CHEBI:10836"],
    ),
    GeneralSearchData(
        search_term=Term(term="73692510"), name="cross_references_cas_registry_number", expected_results=["CHEBI:10836"]
    ),
    GeneralSearchData(
        search_term=Term(term="C15525"), name="cross_references_kegg", expected_results=["CHEBI:10836", "CHEBI:48967"]
    ),
    GeneralSearchData(
        search_term=Term(term="LSM-2929"), name="cross_references_lsm", expected_results=["CHEBI:164200"]
    ),
    GeneralSearchData(search_term=Term(term="LSM2929"), name="cross_references_lsm", expected_results=["CHEBI:164200"]),
    GeneralSearchData(search_term=Term(term="lsm2929"), name="cross_references_lsm", expected_results=["CHEBI:164200"]),
    GeneralSearchData(search_term=Term(term="C10H7NO4"), name="formula", expected_results=["CHEBI:10072"]),
    GeneralSearchData(
        search_term=Term(term="blood"),
        name="match_coincidence_all_places",
        expected_results=[
            "CHEBI:10072",
            "CHEBI:16134",
            "CHEBI:16189",
            "CHEBI:16469",
            "CHEBI:16716",
            "CHEBI:30746",
            "CHEBI:35526",
            "CHEBI:48422",
            "CHEBI:28088",
        ],
    ),
    GeneralSearchData(
        search_term=Term(term="xanthu"),
        name="incomplete_term_match_coincidences_all_places",
        expected_results=["CHEBI:71201", "CHEBI:10072", "CHEBI:171659"],
    ),
    GeneralSearchData(
        search_term=Term(term="hydroxybenz*"),
        name="wildcard_suffix_match_coincidences_all_places",
        expected_results=["CHEBI:24675", "CHEBI:24676", "CHEBI:24679", "CHEBI:152051", "CHEBI:218642"],
    ),
    GeneralSearchData(
        search_term=Term(term="*hydroxybenz"),
        name="wildcard_prefix_match_coincidences_all_places",
        expected_results=[
            "CHEBI:24675",
            "CHEBI:24676",
            "CHEBI:24679",
            "CHEBI:152051",
            "CHEBI:218642",
            "CHEBI:16204",
            "CHEBI:22707",
            "CHEBI:64398",
        ],
    ),
]
