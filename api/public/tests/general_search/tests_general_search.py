import pytest

from api.public.tests.general_search.use_cases import general_search_test_cases
from api.public.tests.markers import require_es_connection


@pytest.mark.parametrize(
    "general_search_data",
    general_search_test_cases,
    ids=lambda general_search_data: general_search_data.name,
)
@require_es_connection
def test_general_search(max_results_for_testing, general_search_data, test_client):
    response = test_client.get(
        "http://localhost:8000/api/public/es_search",
        params={**general_search_data.search_term.model_dump(), "size": max_results_for_testing},
    )
    results = response.json()
    document_compounds_ids = list(document["_source"]["chebi_accession"] for document in results["results"])
    assert response.status_code == 200
    assert {"total", "number_pages", "results"} == set(results.keys())
    assert results["total"] == general_search_data.expected_total
    assert sorted(document_compounds_ids) == sorted(general_search_data.expected_results)
