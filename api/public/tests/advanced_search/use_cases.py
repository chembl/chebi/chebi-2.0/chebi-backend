from dataclasses import dataclass, field

from pydantic import BaseModel

from pydantic_models.advanced_search import (
    DatabaseNameSpecification,
    FormulaSpecification,
    FullSpecification,
    Nested,
    Ontology,
    OntologySpecification,
    Range,
    RangeSpecification,
    Term,
    Text,
    TextSearchSpecification,
)


@dataclass
class SimpleSpecificationData:
    specification: BaseModel | None = None
    specification_field: str | None = None
    expected_results: list[str | int] = field(default_factory=list)
    expected_total: int = 0
    name: str | None = None
    full_specification: FullSpecification | None = None

    # Parameters sent by the URL
    three_star_only: bool = True

    def __post_init__(self):
        self.expected_total = len(self.expected_results)
        body = {self.specification_field: self.specification}
        self.full_specification = FullSpecification(**{k: v for k, v in body.items() if k is not None})
        if not self.name:
            self.name = f"simple_{self.specification_field}"


@dataclass
class ComplexSpecificationData:
    specifications: list[SimpleSpecificationData]
    expected_results: list[str]
    name: str
    expected_total: int = 0
    full_specification: FullSpecification | None = None

    # Parameters sent by the URL
    three_star_only: bool = True

    def __post_init__(self):
        spec = {
            simple_specification.specification_field: simple_specification.specification
            for simple_specification in self.specifications
        }
        self.full_specification = FullSpecification(**spec)
        self.expected_total = len(self.expected_results)


# Test cases used by unit tests.
specification_test_cases = [
    SimpleSpecificationData(
        three_star_only=False,
        name="text_search_all_results",
        expected_results=list(range(516)),
    ),
    # Get all stars compounds with dehydro in its name.
    SimpleSpecificationData(
        name="text_specification_3_stars",
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="dehydro", category="name")]),
        expected_results=["CHEBI:138950", "CHEBI:139472"],
    ),
    # Get all 3 stars compounds with dehydro in its name.
    SimpleSpecificationData(
        name="text_specification_all_stars",
        three_star_only=False,
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="dehydro", category="name")]),
        expected_results=["CHEBI:138950", "CHEBI:139472", "CHEBI:67427"],
    ),
    # Get all 3 and 2 stars compounds with xanthur* in its name.
    SimpleSpecificationData(
        name="text_specification_all_stars_WILDCARD",
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="xanthur*", category="name")]),
        expected_results=["CHEBI:10072", "CHEBI:71201"],
    ),
    SimpleSpecificationData(
        specification_field="mass_specification",
        specification=RangeSpecification(and_specification=[Range(init_range=80, final_range=100)]),
        expected_results=[
            "CHEBI:139371",
            "CHEBI:139520",
            "CHEBI:16189",
            "CHEBI:26819",
            "CHEBI:26836",
            "CHEBI:33853",
            "CHEBI:37581",
            "CHEBI:45696",
            "CHEBI:58958",
        ],
    ),
    SimpleSpecificationData(
        specification_field="charge_specification",
        specification=RangeSpecification(and_specification=[Range(init_range=-2, final_range=-1)]),
        expected_results=[
            "CHEBI:10545",
            "CHEBI:134346",
            "CHEBI:134386",
            "CHEBI:138950",
            "CHEBI:139371",
            "CHEBI:13941",
            "CHEBI:142839",
            "CHEBI:16150",
            "CHEBI:16189",
            "CHEBI:29067",
            "CHEBI:29337",
            "CHEBI:29340",
            "CHEBI:35757",
            "CHEBI:45696",
            "CHEBI:58636",
            "CHEBI:58958",
            "CHEBI:71201",
            "CHEBI:74224",
            "CHEBI:77621",
            "CHEBI:91007",
        ],
    ),
    SimpleSpecificationData(
        specification_field="monoisotopicmass_specification",
        specification=RangeSpecification(and_specification=[Range(init_range=44.5, final_range=70.5)]),
        expected_results=[
            "CHEBI:134361",
            "CHEBI:134396",
            "CHEBI:13941",
            "CHEBI:28616",
            "CHEBI:33575",
            "CHEBI:46883",
            "CHEBI:51689",
        ],
    ),
    SimpleSpecificationData(
        specification_field="formula_specification",
        specification=FormulaSpecification(and_specification=[Term(term="C10H7NO4")]),
        expected_results=["CHEBI:10072"],
    ),
    SimpleSpecificationData(
        specification_field="database_name_specification",
        specification=DatabaseNameSpecification(and_specification=[Term(term="HMDB")]),
        three_star_only=False,
        expected_results=[
            "CHEBI:10072",
            "CHEBI:134254",
            "CHEBI:16134",
            "CHEBI:16150",
            "CHEBI:16189",
            "CHEBI:16204",
            "CHEBI:16469",
            "CHEBI:16716",
            "CHEBI:28088",
            "CHEBI:30746",
            "CHEBI:48569",
            "CHEBI:48965",
            "CHEBI:71201",
            "CHEBI:87620",
            "CHEBI:174747",
            "CHEBI:65",
        ],
        name="all_compounds_HMDB_database",
    ),
    # Advanced Text Queries
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="22723", category="chebi_accession")]),
        expected_results=["CHEBI:22723"],
        name="advanced_text_search_chebi_id",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="CHEBI:22723", category="chebi_accession")]),
        expected_results=["CHEBI:22723"],
        name="advanced_text_search_chebi_id",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="chebi22723", category="chebi_accession")]),
        expected_results=["CHEBI:22723"],
        name="advanced_text_search_chebi_id",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="chebi:22723", category="chebi_accession")]),
        expected_results=["CHEBI:22723"],
        name="advanced_text_search_chebi_id",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="5-(3',4'-dihydroxyphenyl)-gamma-valerolactone 3'-O-sulfate", category="name")]
        ),
        expected_results=["CHEBI:134254"],
        name="advanced_text_search_chebi_name",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="17β-hydroxy steroid", category="name")]),
        expected_results=["CHEBI:35343", "CHEBI:36838"],
        name="advanced_text_search_chebi_name",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="17beta-hydroxy steroid", category="name")]),
        expected_results=["CHEBI:35343"],
        name="advanced_text_search_chebi_name",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[
                Text(text="EC 5.99.1.3 [DNA topoisomerase (ATP-hydrolysing)] inhibitor", category="name")
            ]
        ),
        expected_results=["CHEBI:50750"],
        name="advanced_text_search_chebi_name",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="organic tricyclic compound", category="name")]
        ),
        expected_results=["CHEBI:51959"],
        name="advanced_text_search_chebi_name",
    ),
    # IUPAC_NAME
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="organic*", category="name")]),
        expected_results=[
            "CHEBI:64775",
            "CHEBI:134179",
            "CHEBI:77853",
            "CHEBI:64769",
            "CHEBI:24532",
            "CHEBI:25693",
            "CHEBI:25696",
            "CHEBI:25697",
            "CHEBI:25699",
            "CHEBI:25704",
            "CHEBI:33245",
            "CHEBI:33247",
            "CHEBI:33659",
            "CHEBI:33822",
            "CHEBI:33832",
            "CHEBI:36587",
            "CHEBI:37175",
            "CHEBI:38163",
            "CHEBI:38166",
            "CHEBI:50047",
            "CHEBI:50860",
            "CHEBI:51958",
            "CHEBI:51959",
            "CHEBI:64709",
            "CHEBI:72695",
        ],
        name="advanced_text_search_chebi_name_WILDCARD",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[
                Nested(
                    texts=["4,8-dihydroxyquinoline-2-carboxylic acid", "IUPAC_NAME"],
                    categories=["synonyms.name", "synonyms.type"],
                )
            ]
        ),
        expected_results=["CHEBI:10072"],
        name="advanced_text_search_IUPAC_NAME",
    ),
    # BRAND_NAME
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[
                Nested(texts=["Sophoricol", "BRAND_NAME"], categories=["synonyms.name", "synonyms.type"])
            ]
        ),
        expected_results=["CHEBI:28088"],
        name="advanced_text_search_BRAND_NAME",
    ),
    # INN
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Nested(texts=["triclosán", "INN"], categories=["synonyms.name", "synonyms.type"])]
        ),
        expected_results=["CHEBI:164200"],
        name="advanced_text_search_INN",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="An azane that consists", category="definition")]
        ),
        expected_results=["CHEBI:16134"],
        name="advanced_text_search_definition",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="hydrogen ion(s)", category="definition")]),
        expected_results=["CHEBI:24833"],
        name="advanced_text_search_definition",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="(R' = H) or ketones (R' ≠ H).", category="definition")]
        ),
        expected_results=["CHEBI:33823"],
        name="advanced_text_search_definition",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="[1*]/C([2*])=C([3*])C([4*])([5*])O", category="smiles")]
        ),
        expected_results=["CHEBI:134361"],
        name="advanced_text_search_smiles",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="O=c1c2*", category="smiles")]),
        three_star_only=False,
        expected_results=[
            "CHEBI:27945",
            "CHEBI:31080",
            "CHEBI:37647",
            "CHEBI:58636",
            "CHEBI:65480",
            "CHEBI:80397",
            "CHEBI:65",
        ],
        name="advanced_text_search_smiles_WILDCARD",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[
                Text(
                    text="InChI=1S/C15H12O6/c16-8-3-1-7(2-4-8)15-14(20)13(19)12-10(18)5-9(17)6-11(12)21-15/h1-6,13,16-20H/p-1/t13-/m0/s1",
                    category="inchi",
                )
            ]
        ),
        expected_results=["CHEBI:138950"],
        name="advanced_text_search_inchi",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="GMICVRNSOOSANN-ZDUSSCGKSA-M", category="inchikey")]
        ),
        expected_results=["CHEBI:138950", "CHEBI:139472"],
        name="advanced_text_search_inchi_full",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="GMICVRNSOOSANN", category="inchikey")]),
        expected_results=["CHEBI:138950", "CHEBI:139472"],
        name="advanced_text_search_inchi_first_part",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="GMICVRNSOOSANN-ZDUSSCGKSA", category="inchikey")]
        ),
        expected_results=["CHEBI:138950", "CHEBI:139472"],
        name="advanced_text_search_inchi_second_part",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="C10H7NO4", category="formula")]),
        expected_results=["CHEBI:10072"],
        name="advanced_text_search_formula",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="C10*", category="formula")]),
        expected_results=["CHEBI:10072", "CHEBI:71201"],
        name="advanced_text_search_formula_WILDCARD",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="-2", category="charge")]),
        expected_results=["CHEBI:16189", "CHEBI:29340"],
        name="advanced_text_search_charge",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="246.438", category="mass")]),
        expected_results=["CHEBI:23966"],
        name="advanced_text_search_mass",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="261.22184", category="monoisotopicmass")]),
        expected_results=["CHEBI:35343"],
        name="advanced_text_search_monoisotopicmass",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            or_specification=[Text(text="ketone", category="name"), Text(text="alcohol", category="name")],
            but_not_specification=[Text(text=-1, category="charge")],
        ),
        expected_results=[
            "CHEBI:17087",
            "CHEBI:30879",
            "CHEBI:3992",
            "CHEBI:76224",
            "CHEBI:134361",
            "CHEBI:15734",
            "CHEBI:24679",
            "CHEBI:33854",
            "CHEBI:35681",
            "CHEBI:51721",
            "CHEBI:134396",
            "CHEBI:33857",
            "CHEBI:22743",
        ],
        name="advanced_text_or_but_not_and",
    ),
    # Registry Numbers
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="7664-41-7", category="database_references")]
        ),
        expected_results=["CHEBI:16134"],
        name="advanced_text_search_xref_registry_numbers",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="7664417", category="database_references")]),
        expected_results=["CHEBI:16134"],
        name="advanced_text_search_xref_registry_numbers",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="CAS7664417", category="database_references")]
        ),
        expected_results=["CHEBI:16134"],
        name="advanced_text_search_xref_registry_numbers",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="cas7664417", category="database_references")]
        ),
        expected_results=["CHEBI:16134"],
        name="advanced_text_search_xref_registry_numbers",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="cas7664-41-7", category="database_references")]
        ),
        expected_results=["CHEBI:16134"],
        name="advanced_text_search_xref_registry_numbers",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="Reaxys300962", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_registry_numbers",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="reaxys300962", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_registry_numbers",
    ),
    # Citation
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="17222819", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_citation",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="PMID17222819", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_citation",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="pmid17222819", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_citation",
    ),
    # Automatic Xref
    # Manual xref
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="LINCSLSM-4042", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_manual",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="LINCSLSM4042", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_manual",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="lincslsm-4042", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_manual",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="lincslsm4042", category="database_references")]
        ),
        expected_results=["CHEBI:2674"],
        name="advanced_text_search_xref_manual",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="HMDB", category="database_references")],
        ),
        three_star_only=False,
        expected_results=[
            "CHEBI:10072",
            "CHEBI:134254",
            "CHEBI:16134",
            "CHEBI:16150",
            "CHEBI:16189",
            "CHEBI:16204",
            "CHEBI:16469",
            "CHEBI:16716",
            "CHEBI:28088",
            "CHEBI:30746",
            "CHEBI:48569",
            "CHEBI:48965",
            "CHEBI:71201",
            "CHEBI:87620",
            "CHEBI:174747",
            "CHEBI:65",
        ],
        name="advanced_text_search_xref_manual_all_HMDB",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="HMDB", category="database_references"), Text(text=-1, category="charge")],
        ),
        expected_results=["CHEBI:16150", "CHEBI:71201"],
        name="advanced_text_search_xref_manual_HMDB_with_particular_charge",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[
                Text(text="HMDB", category="database_references"),
            ],
            but_not_specification=[Text(text=0, category="charge")],
        ),
        expected_results=["CHEBI:16150", "CHEBI:16189", "CHEBI:71201"],
        name="advanced_text_search_xref_manual_HMDB_without_a_particular_charge",
    ),
    # Compound species
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Nested(texts=["homo sApIens"], categories=["compound_origins.all"])]
        ),
        three_star_only=False,
        expected_results=[
            "CHEBI:10072",
            "CHEBI:134254",
            "CHEBI:16134",
            "CHEBI:16189",
            "CHEBI:16204",
            "CHEBI:16469",
            "CHEBI:16716",
            "CHEBI:28088",
            "CHEBI:28938",
            "CHEBI:30746",
            "CHEBI:48569",
            "CHEBI:87593",
            "CHEBI:87598",
            "CHEBI:87620",
            "CHEBI:167879",
            "CHEBI:192972",
        ],
        name="compounds_species_all",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Nested(texts=["metabolights"], categories=["compound_origins.comments"])]
        ),
        expected_results=[
            "CHEBI:10072",
            "CHEBI:134254",
            "CHEBI:16134",
            "CHEBI:16150",
            "CHEBI:16189",
            "CHEBI:16469",
            "CHEBI:28088",
            "CHEBI:28616",
            "CHEBI:30746",
        ],
        name="compounds_species_comments",
    ),
    # All Synonyms Name
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Nested(texts=["amodiaquine"], categories=["synonyms.all"])]
        ),
        expected_results=["CHEBI:2674", "CHEBI:131327"],
        name="synonyms_all",
    ),
    # All Names
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            or_specification=[
                Nested(texts=["amodiaquine"], categories=["synonyms.all"]),
                Text(text="amodiaquine", category="name"),
            ]
        ),
        expected_results=["CHEBI:2674", "CHEBI:131327"],
        name="all_names",
    ),
    # All category
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="quino*", category="all")]),
        expected_results=[
            "CHEBI:26512",
            "CHEBI:38773",
            "CHEBI:23765",
            "CHEBI:171659",
            "CHEBI:26513",
            "CHEBI:62872",
            "CHEBI:10072",
            "CHEBI:24646",
            "CHEBI:2674",
            "CHEBI:36709",
            "CHEBI:71201",
            "CHEBI:133170",
            "CHEBI:133233",
            "CHEBI:51071",
            "CHEBI:51072",
        ],
        name="text_search_all_WILDCARD",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="*", category="all")]),
        three_star_only=False,
        expected_results=list(range(0, 516)),
        name="text_search_all_results",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(and_specification=[Text(text="species", category="all")]),
        expected_results=[
            "CHEBI:83056",
            "CHEBI:22563",
            "CHEBI:36916",
            "CHEBI:131327",
            "CHEBI:133233",
            "CHEBI:91007",
            "CHEBI:64915",
            "CHEBI:77621",
            "CHEBI:133170",
            "CHEBI:152051",
        ],
        name="text_search_all_species",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[Text(text="species", category="all"), Text(text=-1, category="charge")]
        ),
        expected_results=["CHEBI:91007", "CHEBI:77621"],
        name="text_search_special_all_species_with_particular_charge",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            and_specification=[
                Text(text="species", category="all"),
            ],
            but_not_specification=[Text(text=-1, category="charge"), Text(text=1, category="charge")],
        ),
        expected_results=[
            "CHEBI:83056",
            "CHEBI:22563",
            "CHEBI:36916",
            "CHEBI:64915",
            "CHEBI:133170",
            "CHEBI:152051",
        ],
        name="text_search_special_all_species_without_some_charges",
    ),
    SimpleSpecificationData(
        specification_field="text_search_specification",
        specification=TextSearchSpecification(
            or_specification=[Text(text="major species", category="all"), Text(text="homo*", category="all")],
        ),
        expected_results=[
            "CHEBI:33597",
            "CHEBI:35295",
            "CHEBI:77746",
            "CHEBI:33598",
            "CHEBI:50699",
            "CHEBI:72544",
            "CHEBI:131327",
            "CHEBI:133233",
            "CHEBI:91007",
            "CHEBI:77621",
            "CHEBI:133170",
        ],
        name="text_search_special_all_major_species_OR_homo_WILDCARD",
    ),
    # Ontology queries related.
    # IS A quinolines and IS A phenols BUT NOT (IS A) antimalarial
    SimpleSpecificationData(
        specification_field="ontology_specification",
        specification=OntologySpecification(
            and_specification=[
                Ontology(relation="is_a", entity="CHEBI:26513"),
                Ontology(relation="is_a", entity="CHEBI:33853"),
            ],
            but_not_specification=[Ontology(relation="is_a", entity="CHEBI:38068")],
        ),
        expected_results=["CHEBI:133170", "CHEBI:171659", "CHEBI:2674", "CHEBI:66901"],
        name="is_a_quinoles_and_is_a_phenols_but_not_is_a_antimalarial",
    ),
    # IS A phenols and Average Mass range from 288 to 290 BUT NOT (HAS ROLE) plant metabolite
    ComplexSpecificationData(
        specifications=[
            SimpleSpecificationData(
                specification_field="ontology_specification",
                specification=OntologySpecification(
                    and_specification=[Ontology(relation="is_a", entity="CHEBI:33853")],  # phenols
                    but_not_specification=[Ontology(relation="has_role", entity="CHEBI:76924")],  # plant metabolite
                ),
            ),
            SimpleSpecificationData(
                specification_field="mass_specification",
                specification=RangeSpecification(and_specification=[Range(init_range=288, final_range=290)]),
            ),
        ],
        three_star_only=False,
        expected_results=[
            "CHEBI:134254",
            "CHEBI:136850",
            "CHEBI:139472",
            "CHEBI:152051",
            "CHEBI:152052",
            "CHEBI:152053",
            "CHEBI:164200",
            "CHEBI:171670",
            "CHEBI:31080",
            "CHEBI:48967",
            "CHEBI:64398",
            "CHEBI:65932",
            "CHEBI:80397",
            "CHEBI:87593",
            "CHEBI:87598",
            "CHEBI:87620",
            "CHEBI:9368",
            "CHEBI:174747",
            "CHEBI:203191",
            "CHEBI:203632",
            "CHEBI:212376",
            "CHEBI:213639",
            "CHEBI:218642",
            "CHEBI:223442",
            "CHEBI:4492",
            "CHEBI:65",
            "CHEBI:67427",
            "CHEBI:93094",
        ],
        name="is_a_phenol_but_not_has_role_plant_metabolite_and_mass_288_290",
    ),
    ComplexSpecificationData(
        specifications=[
            SimpleSpecificationData(
                specification_field="formula_specification",
                specification=FormulaSpecification(and_specification=[Term(term="C6H12O7")]),
            ),
            SimpleSpecificationData(
                specification_field="ontology_specification",
                specification=OntologySpecification(
                    but_not_specification=[
                        Ontology(relation="is_a", entity="CHEBI:78616")
                    ]  # CHEBI:78616 = (carbohydrates and carbohydrate derivatives)
                ),
            ),
        ],
        three_star_only=False,
        expected_results=["CHEBI:167879", "CHEBI:192972"],
        name="compounds_with_formula_C6H12O7_but_not_is_a_carbohydrates_and_carbohydrate_derivatives",
    ),
]
