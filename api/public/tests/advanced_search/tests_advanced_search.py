import pytest
from api.public.tests.markers import require_es_connection
from api.public.tests.advanced_search.use_cases import (
    ComplexSpecificationData,
    SimpleSpecificationData,
    specification_test_cases,
)
from unittest.mock import patch


@pytest.mark.parametrize(
    "request_body, detail_message",
    [
        ({"id": 0, "name": "hello"}, "Your body request has wrong values"),
        ({"stars": []}, "List should have at least 1 item after validation"),
        ({"stars": [4]}, "You can search only compounds with 2 or 3 stars"),
        ({"stars": [2, 3, 4]}, "List should have at most 2 items after validation"),
        (
            {"id": 0, "formula_specification": {"and_specification": [{"term": "H2O"}]}},
            "Your body request has wrong values",
        ),
        # validations using TextSearchSpecification with Nested
        (
            {
                "text_search_specification": {
                    "and_specification": [{"texts": ["water"], "categories": ["synonyms.all", "names"]}]
                }
            },
            "You only can search a text in only one specific field",
        ),
        (
            {
                "text_search_specification": {
                    "and_specification": [
                        {"texts": ["water", "IUPAC_NAME"], "categories": ["synonyms.all", "synonyms.type"]}
                    ]
                }
            },
            "You can use .all to perform a searching over one elasticsearch nested field only",
        ),
        (
            {
                "text_search_specification": {
                    "and_specification": [{"texts": ["water"], "categories": ["undefined_field"]}]
                }
            },
            "Categories field must include an elasticsearch nested filed",
        ),
        (
            {
                "text_search_specification": {
                    "and_specification": [{"texts": ["water"], "categories": ["compound_origins"]}]
                }
            },
            "you must use a dot (.) to indicate an inner elasticsearch field.",
        ),
        (
            {
                "text_search_specification": {
                    "and_specification": [
                        {"texts": ["water", "IUPAC_NAME"], "categories": ["name.lower", "synonyms.type"]}
                    ]
                }
            },
            "You must use the same nested elasticsearch field along a list",
        ),
    ],
    ids=[
        "no_specification",
        "empty_star",
        "invalid_star_number",
        "invalid_number_of_stars",
        "specification_and_bad_key",
        "nested_error_number_items",
        "nested_error_all_operator",
        "nested_error_invalid_elasticsearch_field",
        "nested_error_not_dot_field",
        "nested_error_different_fields",
    ],
)
@require_es_connection
def test_wrong_request_body(request_body, detail_message, test_client):
    with patch("chebi_models.models.compound.RelationType.objects"):
        response = test_client.post("http://localhost:8000/api/public/advanced_search/", json=request_body)
        assert response.status_code == 400
        print(response.json()["detail"])
        assert detail_message in response.json()["detail"]


@pytest.mark.parametrize(
    "specification_data",
    specification_test_cases,
    ids=lambda specification_data: specification_data.name,
)
@require_es_connection
def test_specifications(
    specification_data: SimpleSpecificationData | ComplexSpecificationData, max_results_for_testing, test_client
):
    """This unit test aims testing all specifications used by the advanced test, specifically we want to test:
    - Term Specification: which includes formula and database_name
    - Range Specification: which includes mass, monoisotopic_mass and charge
    - Ontology Specification: which includes is_a and has_role, two of the most used ontology relations
    - Text Search Specification: Which includes all the possible categories"""

    with patch("chebi_models.models.compound.RelationType.objects"):
        response = test_client.post(
            "http://localhost:8000/api/public/advanced_search/",
            json=specification_data.full_specification.model_dump(),
            params={"size": max_results_for_testing, "three_star_only": specification_data.three_star_only},
        )
        results = response.json()
        document_compounds_ids = list(document["_source"]["chebi_accession"] for document in results["results"])
        assert response.status_code == 200
        assert {"total", "number_pages", "results"} == set(results.keys())
        assert results["total"] == specification_data.expected_total

        if specification_data.name.startswith("text_search_all_results"):
            assert len(document_compounds_ids) == len(specification_data.expected_results)
        else:
            assert sorted(document_compounds_ids) == sorted(specification_data.expected_results)
