from itertools import zip_longest

from elasticsearch_dsl import Q as EQ

from api.public.elastic_queries.simple_text_search import get_keywords_query, get_text_search_query
from pydantic_models.advanced_search import Nested, Text


def get_queries_for_text_search(text_search_item: Text | Nested) -> list[EQ]:
    """
    Returns all queries that can be used to perform a text query. Item received can be Text -
    for example: Text(text="H2O", category="formula") or Nested, for example:
    Nested(texts=["water", "IUPAC_NAME], categories=["synonyms.name", "synonyms.type"])
    Different elasticsearch queries are created depending on the above item types.

    Also, it is having into account the category's elasticsearch mapping. For example, Text(text="H2O", category="formula")
    has "formula" as its category, in the elasticsearch document, "formula" is a "keyword" and it has subfields:
    "formula.lower" and "formula.special", both the field type and the subfields are considered to create elasticsearch queries.

    :param text_search_item: Text or Nested object to be used to perform a text query
    :return: List of Elasticsearch queries that can be used to perform a text query
    """
    term = text_search_item.text if isinstance(text_search_item, Text) else text_search_item.texts
    elastic_field = text_search_item.category if isinstance(text_search_item, Text) else text_search_item.categories
    elastic_mapping = text_search_item.field_mapping  # Elasticsearch information related to category
    elastic_field_type = elastic_mapping["type"] if "type" in elastic_mapping else None
    elastic_queries = []

    if elastic_field == "all":
        # If category is all, then we perform a text query using the same strategy used by simple text search (main search bar in the frontend)
        elastic_queries += [EQ("bool", should=[get_keywords_query(term), get_text_search_query(term)])]
    elif elastic_field_type in ["keyword", "float", "integer"]:
        elastic_queries += (
            [
                EQ("term", **{elastic_field: {"value": term, "boost": 2}}),
                EQ("term", **{"_id": {"value": term, "boost": 2.5}}),
            ]
            if elastic_field == "chebi_accession"
            else [EQ("term", **{elastic_field: {"value": term, "boost": 2}})]
        )
    elif elastic_field_type == "text":
        # If the field type is Text, then we store two queries: wildcard and match query
        wildcard_term = f"*{term}*" if "*" not in term else term
        elastic_queries += [
            EQ("wildcard", **{elastic_field: wildcard_term}),
            EQ("match_phrase", **{elastic_field: term}),
        ]

    elastic_queries = get_queries_for_subfields(elastic_mapping, elastic_field, elastic_queries, term)

    if elastic_field_type == "nested":
        # If the field type is Nested, then we store an elasticsearch-nested query, this is a
        # special case because, for nested types, term and field variables are both lists.
        nested_field: str = text_search_item.nested_field
        nested_sub_fields: list = text_search_item.nested_sub_fields
        # is_all: If a client sends a nested expression using .all special value, or it doesn't
        is_all: bool = text_search_item.is_all

        queries = [
            EQ("match_phrase", **{f"{nested_field}.{sub_field}": my_term})
            for my_term, sub_field in zip_longest(term, nested_sub_fields, fillvalue=term[0])
        ]

        elastic_queries += [
            (
                EQ("nested", path=nested_field, query=EQ("bool", should=queries))
                if is_all
                else EQ("nested", path=nested_field, query=EQ("bool", must=queries))
            )
        ]

    return elastic_queries


def get_queries_for_subfields(
    elastic_mapping: dict, elastic_field: str, elastic_queries: list[EQ], term: str | int
) -> list[EQ]:
    """
    Function to execute text queries over all subfields of the Elasticsearch field types Keyword, Integer, Float and Text
    :param elastic_mapping: Elasticsearch mapping of the category
    :param elastic_field: Elasticsearch field name
    :param term: text used to search.
    :param elastic_queries: list of elasticsearch queries stored until now.
    :return: list of EQ queries applied into subfields
    """
    sub_fields = elastic_mapping.get("fields", dict())
    queries = []
    # We check if field has sub_fields and which are their types, for Nested types, this loop won't be
    # executed.
    for sub_field_name, sub_field_type in sub_fields.items():
        if sub_field_type["type"] in ["keyword", "float", "integer"]:
            queries += [
                EQ("wildcard", **{f"{elastic_field}.{sub_field_name}": term}),
                EQ("term", **{f"{elastic_field}.{sub_field_name}": {"value": term, "boost": 1.7}}),
            ]
        elif sub_field_type["type"] == "text":
            wildcard_term = f"*{term}*" if "*" not in term else term
            queries += [
                EQ("wildcard", **{f"{elastic_field}.{sub_field_name}": wildcard_term}),
                EQ("match_phrase", **{f"{elastic_field}.{sub_field_name}": term}),
            ]
    # If there are queries for subfields, then we create a Boolean Should EQ with the subfields queries + elastic_queries.
    # Otherwise, we return the same elastic_queries
    if len(queries) > 0:
        return [EQ("bool", should=(queries + elastic_queries))]
    return elastic_queries
