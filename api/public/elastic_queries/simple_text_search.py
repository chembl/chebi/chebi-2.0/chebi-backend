"""
Queries related to simple text search, it is the main search bar on the new ChEBI website
"""

from elasticsearch_dsl import Q as EQ
from elasticsearch_dsl.search import Search

from api.utils.es_responses import MinimalCompoundResults
from api.utils.es_search_fields import KEYWORDS_SEARCH_FIELDS, TEXT_SEARCH_FIELDS
from base.es_settings import ES_SEARCH

# these are the keyword used by performing an exact query (try to get 1 compound)
keywords_search_fields = [
    (nested_field, search_fields)
    for nested_field, search_fields in KEYWORDS_SEARCH_FIELDS.items()
    if nested_field != "outer_fields"
]

text_search_fields = [
    (nested_field, search_fields)
    for nested_field, search_fields in TEXT_SEARCH_FIELDS.items()
    if nested_field != "outer_fields"
]


def get_keywords_query(term: str) -> EQ:
    """
    Perform an exact query by keyword, we will try to get one compound!
    """
    wildcard_term = "*" if term.strip() == "*" else f"*{term}*"
    # initial value of exact_query variable is an elasticsearch query of type 'IDs':
    # https://www.elastic.co/guide/en/elasticsearch/reference/7.5/query-dsl-ids-query.html
    exact_query = (
        EQ("ids", values=[term]) | EQ("wildcard", formula=wildcard_term) | EQ("wildcard", formula__lower=wildcard_term)
    ) | EQ("wildcard", formula__special=wildcard_term)
    for keyword, my_boost in KEYWORDS_SEARCH_FIELDS.get("outer_fields").items():
        exact_query |= EQ("term", **{keyword: {"value": term, "boost": my_boost}})

    for nested_field, search_fields in keywords_search_fields:
        for keyword, my_boost in search_fields.items():
            exact_query |= EQ(
                "nested",
                path=nested_field,
                query=EQ("term", **{f"{nested_field}.{keyword}": {"value": term, "boost": my_boost}}),
            )

    return exact_query


def get_wildcard_query(term: str) -> EQ:
    """
    This function executes a wildcard query, term string should have one or more '*'.
    It is used by the search text process.
    """

    wildcard_query = [
        EQ("wildcard", **{name_keys: term}) for name_keys in [i for i in TEXT_SEARCH_FIELDS.get("outer_fields").keys()]
    ]

    for nested_field, search_fields in text_search_fields:
        for field, _ in search_fields.items():
            wildcard_query += [
                EQ(
                    "nested",
                    path=nested_field,
                    query=EQ("wildcard", **{f"{nested_field}.{field}": term}),
                )
            ]

    wildcard_query = EQ("bool", should=wildcard_query)

    return wildcard_query


def get_text_search_query(term: str) -> EQ:
    """
    Perform a search query by free text following these steps:
    1. If the searching term has one or more '*' character, then it is performed a wildcard query
    2. Otherwise, we execute a multi_match query assigning boots on each field.
    3. Finally, if the multi_match query does not get any results, we force to perform a wildcard query setting
    *term* as the new searching term.
    """

    # Firstly, we perform a wildcard query if it is the case
    # A wildcard query can be performed only over a single term: https://stackoverflow.com/questions/30113753/elastic-search-wildcard-search-with-spaces
    # So this is valid: 'organic*', but this is not: 'An organic*', for the above we need to validate if term has only
    # 1 word.
    if "*" in term and len(term.strip().split()) == 1:
        return get_wildcard_query(term)
    elif "*" in term:
        term = term.replace("*", "")

    # Otherwise we perform a multimatch taking into account the outer fields in the ES json documents
    # and the nested ones.
    text_search_query = EQ(
        "multi_match",
        type="phrase",
        query=term,
        fields=[f"{field}^{boots}" for field, boots in TEXT_SEARCH_FIELDS.get("outer_fields").items()],
    )

    for nested_field, search_fields in text_search_fields:
        text_search_query |= EQ(
            "nested",
            path=nested_field,
            query=EQ(
                "multi_match",
                query=term,
                type="phrase",
                fields=[f"{nested_field}.{field}^{boots}" for field, boots in search_fields.items()],
            ),
        )

    return text_search_query


def get_compounds(term: str) -> Search:
    """
    Return the compounds found in the Elasticsearch documents. Firstly, we try to perform an exact match by keyword,
    If the above does not get any compound, then we perform a text search using the fields/multifields/wildcard.
    """
    exact_compound: Search = ES_SEARCH.query(get_keywords_query(term)).response_class(MinimalCompoundResults)
    if not exact_compound.count():
        lower_term = term.lower()
        compounds = ES_SEARCH.query(get_text_search_query(lower_term)).response_class(MinimalCompoundResults)
        print("There is no an exact match!")
        if not compounds.count():
            compounds = ES_SEARCH.query(get_text_search_query(f"*{lower_term}*")).response_class(MinimalCompoundResults)
            if not compounds.count():
                raise ValueError(f"Any compound matches with the {term} you provide")
        return compounds
    return exact_compound
