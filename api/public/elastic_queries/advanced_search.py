import itertools as it
from collections.abc import Callable, Iterable
from dataclasses import dataclass, field

from elasticsearch_dsl import Q as EQ
from elasticsearch_dsl.search import Search

from api.public.elastic_queries.advanced_text_queries import get_queries_for_text_search
from api.utils.es_function_scores import star_function_score
from api.utils.es_responses import MinimalCompoundResults
from base.es_settings import ES_SEARCH
from pydantic_models.advanced_search import (
    FullSpecification,
    OntologySpecification,
    RangeSpecification,
    TermSpecification,
    TextSearchSpecification,
)
from api.utils import chem_structure_search
from pydantic_models.structure_search import StructureSearch

chem_structure_search.get_fpsim_engine()
chem_structure_search.get_rdkit_slib()

# Inspired in the Chain of responsability design pattern:
# https://refactoring.guru/design-patterns/chain-of-responsibility/python/example


@dataclass
class QueriesBySpecification:
    """Class to store the different queries according to the specification."""

    and_specification: list[EQ] = field(default_factory=list)
    or_specification: list[EQ] = field(default_factory=list)
    but_not_specification: list[EQ] = field(default_factory=list)


# Specification handler function must achieve this signature.
SpecificationHandlerFunction = Callable[[FullSpecification, QueriesBySpecification], QueriesBySpecification]


def ontology_specification_handler(
    specification: FullSpecification, queries_by_specification: QueriesBySpecification
) -> QueriesBySpecification:
    """Function to store the elasticsearch queries according to the ontology specification
    :param specification: A pydantic model that represents the full specification to be processed.
    :param queries_by_specification: A Dataclass with three lists, one by each operator.This function will try
    to update those lists using an Elasticsearch query designed to perform ontological searches.
    :return: Return the same queries_by_specification Dataclass, with the updated lists.
    """
    specifications = (
        (property_name, spec) for property_name, spec in specification if isinstance(spec, OntologySpecification)
    )

    for property_name, current_specification in specifications:
        for operator, list_ontology_specification in current_specification:
            elastic_query = [
                (
                    EQ("nested", path="parents", query=EQ("terms", parents__id=ontology_spec.entities))
                    if ontology_spec.relation == "is_a"
                    else EQ(
                        "nested",
                        path=ontology_spec.relation,
                        query=EQ("terms", **{f"{ontology_spec.relation}.id": ontology_spec.entities}),
                    )
                )
                for ontology_spec in list_ontology_specification
            ]
            operator_list = getattr(queries_by_specification, operator)
            operator_list += elastic_query

    return queries_by_specification


def term_specification_handler(
    specification: FullSpecification, queries_by_specification: QueriesBySpecification
) -> QueriesBySpecification:
    """
    Function to store the elasticsearch queries related to the Term specifications, for example: FormulaSpecification
    and DatabaseNameSpecification.
    :param specification: A pydantic model that represents the full specification to be processed.
    :param queries_by_specification: A Dataclass with three lists, one by each operator. This function will try
    to update those lists using an Elasticsearch query designed to perform Term searches.
    :return: Return the same queries_by_specification Dataclass, with the updated lists.
    """
    specifications = (
        (property_name, spec) for property_name, spec in specification if isinstance(spec, TermSpecification)
    )
    for property_name, current_specification in specifications:
        for operator, list_term_specification in current_specification:
            field = property_name.replace("_specification", "")
            field = "database_references.database_name" if field == "database_name" else field
            elastic_query = [
                EQ("term", **{field: f"{term_specification.term}"}) for term_specification in list_term_specification
            ]
            operator_list = getattr(queries_by_specification, operator)
            operator_list += elastic_query
    return queries_by_specification


def range_specification_handler(
    specification: FullSpecification, queries_by_specification: QueriesBySpecification
) -> QueriesBySpecification:
    """
    Function to store the elasticsearch queries related to the Range specifications, for example: MassSpecification,
    ChargeSpecification, MonoisotopicMassSpecification.
    :param specification: A pydantic model that represents the full specification to be processed.
    :param queries_by_specification: A Dataclass with three lists, one by each operator.This function will try
    to update those lists using an Elasticsearch query designed to perform Range searches.
    :return: Return the same queries_by_specification Dataclass, with the updated lists.
    """
    specifications = (
        (property_name, spec) for property_name, spec in specification if isinstance(spec, RangeSpecification)
    )
    for property_name, current_specification in specifications:
        for operator, range_specifications_list in current_specification:
            field = property_name.replace("_specification", "")
            elastic_query = [
                EQ(
                    "range",
                    **{field: {"gte": range_specification.init_range, "lte": range_specification.final_range}},
                )
                for range_specification in range_specifications_list
            ]
            operator_list = getattr(queries_by_specification, operator)
            operator_list += elastic_query

    return queries_by_specification


def text_search_specification_handler(
    specification: FullSpecification, queries_by_specification: QueriesBySpecification
) -> QueriesBySpecification:
    """
    Function to store the elasticsearch queries related to the TextSearch specifications: This type of
    specification it is a bit special, text search needs to be performed not only over Text, but also over Nested,
    Keyword, Integer and Float elasticsearch types.Thus, we can't just reuse the other specification functions here.
    The mentioned ones try to execute a sort of exact query, trying to get closing matches.
    Whereas TextSearch specification will try to find more results using the created subfields inside the main fields
    in Elasticsearch.

    :param specification: A pydantic model that represents the full specification to be processed.
    :param queries_by_specification: A Dataclass with three lists, one by each operator.This function will try
    to update those lists using an Elasticsearch query designed to perform TextSearch searches.
    :return: Return the same queries_by_specification Dataclass, with the updated lists.
    """
    specifications = (
        (property_name, spec) for property_name, spec in specification if isinstance(spec, TextSearchSpecification)
    )
    for property_name, current_specification in specifications:
        for operator, text_search_specifications_list in current_specification:
            # text_search_specifications_list can contain: Text or Nested or a combination of both
            iter_elastic_queries: Iterable[EQ] = map(get_queries_for_text_search, text_search_specifications_list)
            elastic_queries: list[EQ] = list(it.chain.from_iterable(iter_elastic_queries))
            if elastic_queries:
                operator_list = getattr(queries_by_specification, operator)
                operator_list += elastic_queries

    return queries_by_specification


def execute_handlers(
    specification: FullSpecification,
    queries_by_operator: QueriesBySpecification,
    *handlers: SpecificationHandlerFunction,
) -> None:
    """Execute the handlers in order
    :param specification: A pydantic model that represents the full specification to be processed.
    :param queries_by_operator: A Dataclass with three lists, one by each operator
    :param handlers: A handler function to be processed"""
    for handler in handlers:
        handler(specification, queries_by_operator)


def get_compounds(specification: FullSpecification) -> Search:
    queries_by_specification = QueriesBySpecification()
    # Set the handlers to be executed based on the full_specification.We do not need to execute the full
    # path of handlers if a user wants, for example, executing a simple formula_specification.
    handlers = set(
        get_handler_function().get(property_name)
        for property_name, spec in specification
        if spec and property_name.endswith("_specification")
    )
    execute_handlers(specification, queries_by_specification, *handlers)

    struc_must_query = []
    is_similarity = False
    if specification.structure_search:
        struc_must_query = get_structure_search_es_query(specification.structure_search)
        is_similarity = specification.structure_search.type == StructureSearch.SearchType.SIMILARITY

    # Once we have the elasticsearch queries already created in the queries_by_specification Dataclass,
    # the advanced query is assembled here.
    advanced_search_query = EQ(
        "bool",
        must=queries_by_specification.and_specification + struc_must_query,
        must_not=queries_by_specification.but_not_specification,
        should=queries_by_specification.or_specification,
    )
    # Star and has_structure are not a specification, it is just a simple list with 1 or 2 item maximum, and
    # has_structure is a boolean.
    # To include stars and has_structure in the query a filter is used.
    search_query = advanced_search_query if is_similarity else star_function_score(advanced_search_query)

    stars_filter = EQ("bool", must=EQ("terms", **{"stars": specification.stars}))
    has_structure_filter = EQ("bool", must=[], must_not=[])
    if specification.has_structure is not None:
        strut_query = EQ("exists", field="default_structure")
        if specification.has_structure:
            has_structure_filter.must = [strut_query]
        else:
            has_structure_filter.must_not = [strut_query]
    filters = stars_filter + has_structure_filter

    compounds: Search = ES_SEARCH.query(search_query).filter(filters).response_class(MinimalCompoundResults)
    if is_similarity:
        compounds.sort([{"_score": "desc"}, {"stars.integer": "desc"}])
    return compounds


def get_structure_search_es_query(structure_search: StructureSearch) -> list[EQ]:
    # TODO: Set this function as another handler in the same way we program the other queries
    es_query = None
    if structure_search:
        if structure_search.type == StructureSearch.SearchType.CONNECTIVITY:
            es_query = EQ("term", inchikey__first_part=structure_search.get_inchi_connectivity_layer())
        elif structure_search.type == StructureSearch.SearchType.SUBSTRUCTURE:
            chebi_ids = chem_structure_search.get_substructure_chebi_ids(structure_search.get_rdkit_mol())
            es_query = EQ("terms", _id=chebi_ids)
        elif structure_search.type == StructureSearch.SearchType.SIMILARITY:
            chebi_ids_and_sim = chem_structure_search.get_similar_chebi_ids(
                structure_search.get_rdkit_mol(), similarity=structure_search.similarity
            )
            chebi_ids = []
            chebi_ids_2_sim_score = {}
            for id_i, sim_i in chebi_ids_and_sim:
                chebi_id_str = str(id_i)
                chebi_ids.append(chebi_id_str)
                chebi_ids_2_sim_score[chebi_id_str] = sim_i
            es_query = EQ(
                {
                    "script_score": {
                        "query": {"terms": {"_id": chebi_ids}},
                        "script": {"source": "params[doc['_id'].value]", "params": chebi_ids_2_sim_score},
                    }
                }
            )
    if es_query:
        return [es_query]
    return []


def get_handler_function() -> dict[str, SpecificationHandlerFunction]:
    """This method returns a plain dictionary with the pairs type_specification: handler_function.
    Keys in this dictionary must keep the attribute's names of the FullSpecification pydantic model in
    pydantic_models/advanced_search.py"""
    return {
        "ontology_specification": ontology_specification_handler,
        "formula_specification": term_specification_handler,
        "database_name_specification": term_specification_handler,
        "mass_specification": range_specification_handler,
        "monoisotopicmass_specification": range_specification_handler,
        "charge_specification": range_specification_handler,
        "text_search_specification": text_search_specification_handler,
    }
