from django.conf import settings
from django.http import HttpResponse
from django.views import View
from drf_spectacular.utils import OpenApiExample, OpenApiParameter, extend_schema
from libRDChEBI.depiction import depict
from rest_framework.generics import RetrieveAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from api.public.serializers import (
    CompoundDetailSerializer,
    CompoundsFormSerializer,
    ContactFormSerializer,
    PendingSubmissionCompoundSerializer,
    CompoundOntologyOnlySerializer,
)
from base.es_settings import ES_SEARCH
from chebi_models.models import Compound, Structure
from pydantic_models.responses import ResponseAPI
from utils import send_email, standardise_chebi_id, str_to_bool


class CompoundsListAPI(APIView):
    """
    ### Description
    This endpoint allows you to retrieve information about one or more compounds.
    """

    serializer_class = CompoundsFormSerializer
    comp_serializer_class = CompoundDetailSerializer
    max_chebi_ids = 100

    def get_compounds_by_chebi_ids(self, chebi_ids: list[str]):
        standardized_chebi_ids = []
        result_dict = {}
        result_dict_by_standardized_chebi_ids = {}
        for chebi_id_i in chebi_ids:
            standardized_chebi_id_i = standardise_chebi_id(chebi_id_i)
            if standardized_chebi_id_i:
                result_dict[chebi_id_i] = {
                    "standardized_chebi_id": standardized_chebi_id_i,
                    "primary_chebi_id": None,
                    "exists": False,
                    "id_type": None,
                    "data": None,
                }
                result_dict_by_standardized_chebi_ids[standardized_chebi_id_i] = result_dict[chebi_id_i]
                standardized_chebi_ids.append(standardized_chebi_id_i)
        int_chebi_ids = [chebi_id_i.split(":")[1] for chebi_id_i in standardized_chebi_ids]
        qs_chebi_ids = (
            Compound.objects.select_related("parent")
            .prefetch_related("incoming_relations")
            .prefetch_related("outgoing_relations")
            .filter(id__in=int_chebi_ids)
        )
        for compound_j in qs_chebi_ids:
            chebi_id_j = compound_j.chebi_accession
            compound_data_j = compound_j
            type_j = "PRIMARY_ID"
            if compound_j.parent:
                type_j = "SECONDARY_ID"
                compound_data_j = compound_j.parent
            result_dict_by_standardized_chebi_ids[chebi_id_j]["exists"] = True
            result_dict_by_standardized_chebi_ids[chebi_id_j]["id_type"] = type_j
            result_dict_by_standardized_chebi_ids[chebi_id_j]["data"] = self.comp_serializer_class().to_representation(
                compound_data_j
            )
            result_dict_by_standardized_chebi_ids[chebi_id_j]["primary_chebi_id"] = compound_data_j.chebi_accession
        return result_dict

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="chebi_ids",
                description="List of comma separated ChEBI identifiers",
                type=str,
                examples=[OpenApiExample("Request 3 identifiers", value="CHEBI:18357,chebi:22982,5974")],
            ),
        ]
    )
    def get(self, request):
        chebi_ids_str: str = self.request.query_params.get("chebi_ids", "")
        chebi_ids_str = chebi_ids_str.strip()
        if chebi_ids_str:
            chebi_ids = chebi_ids_str.split(",")
            return Response(self.get_compounds_by_chebi_ids(chebi_ids), status=HTTP_200_OK)
        else:
            return Response(
                {
                    "chebi_ids": "The parameter is empty or not specified",
                    "examples": [
                        "WS_URL?chebi_ids=5974" "WS_URL?chebi_ids=chebi:22982",
                        "WS_URL?chebi_ids=CHEBI:18357",
                        "WS_URL?chebi_ids=CHEBI:18357,chebi:22982,5974",
                    ],
                },
                status=HTTP_400_BAD_REQUEST,
            )

    @extend_schema(request=CompoundsFormSerializer)
    def post(self, request):
        serializer = CompoundsFormSerializer(data=request.data)
        if serializer.is_valid():
            validated_data = serializer.validated_data["chebi_ids"]
            return Response(self.get_compounds_by_chebi_ids(validated_data), status=HTTP_200_OK)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class CompoundDetailAPI(RetrieveAPIView):
    """
    ### Description
    This endpoint allows you to retrieve information about a compound.
    """

    only_ontology_parents_param = OpenApiParameter(
        name="only_ontology_parents",
        type=bool,
        description="Return only the ontology Parents.",
        required=False,
        default=False,
    )
    only_ontology_children_param = OpenApiParameter(
        name="only_ontology_children",
        type=bool,
        description="Return only the ontology Children.",
        required=False,
        default=False,
    )

    def parse_ontology_params(self):
        only_ontology_parents: bool = str_to_bool(self.request.query_params.get("only_ontology_parents", "false"))
        only_ontology_children: bool = str_to_bool(self.request.query_params.get("only_ontology_children", "false"))
        return only_ontology_parents, only_ontology_children

    @extend_schema(parameters=[only_ontology_parents_param, only_ontology_children_param])
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

    def get_object(self):
        # Return parent object if exists
        # 0-star and 1-star records are also retrieved in this API
        chebi_id = standardise_chebi_id(self.kwargs.get("chebi_id", ""))
        int_chebi_id = int(chebi_id.split(":")[1])
        compound_obj = get_object_or_404(
            queryset=Compound.objects.filter()
            .prefetch_related("incoming_relations")
            .prefetch_related("outgoing_relations"),
            id=int_chebi_id,
        )
        return compound_obj if not compound_obj.parent else compound_obj.parent

    def retrieve(self, request, *args, **kwargs):
        only_ontology_parents, only_ontology_children = self.parse_ontology_params()
        only_ontology = only_ontology_parents or only_ontology_children

        instance = self.get_object()
        if instance.is_released:
            if only_ontology:
                serializer = CompoundOntologyOnlySerializer(instance, only_ontology_parents, only_ontology_children)
            else:
                serializer = CompoundDetailSerializer(instance)
        else:
            serializer = PendingSubmissionCompoundSerializer(instance)
        return Response(serializer.data)


class CompoundGetOntologyParentsAPI(CompoundDetailAPI):
    def parse_ontology_params(self):
        return True, False

    # Removes the decorator from the parent for DRF Spectacular
    def get(self, *args, **kwargs):
        """Get the parents of a compound"""
        return super().get(*args, **kwargs)


class CompoundGetOntologyChildrenAPI(CompoundDetailAPI):
    def parse_ontology_params(self):
        return False, True

    # Removes the decorator from the parent for DRF Spectacular
    def get(self, *args, **kwargs):
        """Get the children of a compound"""
        return super().get(*args, **kwargs)


class AdvancedSearchFormDataAPI(APIView):
    def get(self, request):
        """Get compounds using Elasticsearch suggester api"""
        ontology_term = request.query_params.get("term", "").capitalize()
        suggestions = ES_SEARCH.suggest(
            "autocomplete", ontology_term, completion={"field": "suggestions", "size": 20}
        ).execute()

        # Pagination not implemented, it is not necessary at all.
        terms = suggestions.suggest["autocomplete"][0].options
        results = list()
        for t in terms:
            source = t["_source"]
            results.append({"value": source["chebi_accession"], "title": source["ascii_name"]})
        response = ResponseAPI(results=results, total=len(results), number_pages=1)
        return Response(response.model_dump(), status=HTTP_200_OK)


class StructureSvgView(View):
    """
    Expects a Primary Key of Structure, and returns raw SVG contents of structure
    """

    def get(self, request, pk):
        width = int(request.GET.get("width", 300))
        height = int(request.GET.get("height", 300))
        structure = get_object_or_404(Structure.public_objects_qs(), pk=pk)
        svg_content = depict(structure.molfile, maxFontSize=50, width=width, height=height)
        return HttpResponse(svg_content, content_type="image/svg+xml")


class MolFileDownloadView(View):
    """
    Expects a Primary Key of Compound, and downloads Mol file of default structure
    Only handles parent compounds
    Returns empty mol file if there is no default structure
    """

    def get(self, request, pk):
        compound = get_object_or_404(Compound, pk=pk)
        response = HttpResponse(
            getattr(compound.default_structure, "molfile", ""), content_type="chemical/x-mdl-molfile"
        )
        response["Content-Disposition"] = f'attachment; filename="{compound.chebi_accession}.mol"'
        return response


class ContactFormView(APIView):
    def post(self, request, *args, **kwargs):
        # TODO: Save the inquiries to database
        # Notify the sender as well via email that we got you inquiry?
        # make it async
        serializer = ContactFormSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        send_email(
            from_email=f'{request.data["email"].split("@")[0]} --- <' + f'{request.data["email"]}>',
            to=settings.CONTACT_EMAIL_TO,
            reply_to=[request.data["email"], settings.CONTACT_EMAIL_TO],
            subject="CHEBI Help",
            body=request.data["message"],
        )
        return Response(serializer.data, status=HTTP_200_OK)
