from typing import Tuple, List

from django.core.paginator import EmptyPage, InvalidPage, Paginator
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_204_NO_CONTENT
from rest_framework.views import APIView

from drf_spectacular.utils import extend_schema, OpenApiParameter
from api.public.elastic_queries.advanced_search import get_compounds as advanced_get_compounds
from api.public.elastic_queries.simple_text_search import get_compounds as simple_get_compounds
from api.utils.es_documentation_endpoints import get_advanced_search_examples
from api.utils.es_download_results import download_es_results
from pydantic_models.advanced_search import (
    FullSpecification,
    OntologySpecification,
    Ontology,
    Star,
)
from pydantic_models.structure_search import StructureSearch
from pydantic_models.responses import ResponseAPI
from chebi_models.models.compound import RelationType
from utils import str_to_bool


class AdvancedSearchAPIBAse(APIView):
    page_number_param = OpenApiParameter(
        name="page", type=int, description="Page number for search results pagination.", required=False, default=1
    )
    page_size_param = OpenApiParameter(
        name="size", type=int, description="Page size for search results pagination.", required=False, default=15
    )
    three_star_only_param = OpenApiParameter(
        name="three_star_only",
        type=bool,
        description="Flag to indicate if the search should be performed in the "
        "whole DB. Defaults to True, and will only include 3 "
        "stars compounds. Use False to get 2 and 3 stars entries.",
        required=False,
        default=True,
    )
    has_structure_param = OpenApiParameter(
        name="has_structure",
        type=bool,
        description="Flag to indicate if the search should include results "
        "with our without structure. If it is not specified, it "
        "will not perform any filtering. Defaults to unspecified.",
        required=False,
        default=None,
    )
    download_param = OpenApiParameter(
        name="download", type=bool, description="Get results in a download format.", required=False, default=False
    )
    general_term = OpenApiParameter(
        name="term", type=str, description="Term to execute the general search.", required=True
    )

    base_query_params_no_has_struct = [three_star_only_param, page_number_param, page_size_param, download_param]
    base_query_params = [three_star_only_param, has_structure_param, page_number_param, page_size_param, download_param]

    def get_base_params_from_request(self) -> Tuple[int, int, List[int], bool, bool | None]:
        page_number = self.request.query_params.get("page", 1)
        page_size = self.request.query_params.get("size", 15)
        three_star_only: bool = str_to_bool(self.request.query_params.get("three_star_only", "true"))
        download: bool = str_to_bool(self.request.query_params.get("download", "false"))
        # Set the stars to take into account. The default number is 3 stars because three_star_only param is True by default
        stars = [Star.THREE_STAR]
        has_structure: bool = str_to_bool(self.request.query_params.get("has_structure", None))
        if not three_star_only:
            stars = [Star.TWO_STAR, Star.THREE_STAR]
        return page_number, page_size, stars, download, has_structure

    def resolve_request_full_specification(self, full_specification: FullSpecification):
        page_number, page_size, stars, download, has_structure = self.get_base_params_from_request()
        response = ResponseAPI()
        try:
            full_specification.stars = stars
            if has_structure is not None:
                full_specification.has_structure = has_structure
            compounds = advanced_get_compounds(full_specification)
            if download:
                return download_es_results(compounds)

            paginated_compounds = Paginator(compounds, page_size)
            results = list(paginated_compounds.page(page_number).object_list)
            response.results, response.total, response.number_pages = (
                results,
                paginated_compounds.count,
                paginated_compounds.num_pages,
            )
            return Response(response.model_dump(), status=HTTP_200_OK)
        except EmptyPage:
            return Response(response.model_dump(), status=HTTP_204_NO_CONTENT)
        except InvalidPage:
            raise ValidationError({"detail": f"Error in page param, its value is: {page_number}"})
        except ValueError as e:
            raise ValidationError({"detail": e})


class ElasticSearchAPI(APIView):
    """
    Perform a text search in the elasticsearch documents, it tries to get an exact match in the first place, otherwise, it
    is executed a text search on the documents. You can use a variety of terms to get compounds.

    - Use a chebi name: `paracetamol`, `α,α-trehalose`, `alpha,alpha-trehalose`
    - Use a brand name: `panadol`
    - Use a IUPAC NAME: `N-(4-hydroxyphenyl)acetamide`
    - Use part of the name: `paraceta`, `paraceta*` or `panad`
    - Use a synonym: `4-acetamidophenol`
    - Use a chebi definition: `A member of the class of phenols that is 4`
    - Use a chebi id: `CHEBI:46195`, `CHEBI46195`, `chebi:46195`, `46195`, `chebi46195`
    - Use a chebi secondary id, for example `CHEBI:2386` that is a secondary id of `CHEBI:46195`
    - Use an inchikey using the full text or part of them, for example: `RZVAJINKPMORJF-UHFFFAOYSA-N`, `RZVAJINKPMORJF` or `RZVAJINKPMORJF-UHFFFAOYSA`
    - Use a formula, inchi and smiles: `C8H9NO2`, `CC(=O)Nc1ccc(O)cc1`, `InChI=1S/C8H9NO2/c1-6(10)9-7-2-4-8(11)5-3-7/h2-5,11H,1H3,(H,9,10)`
    - Use any kind of manual cross-references ids: `HMDB0001859`, `LSM-5533`, `D00217`, `DB00316`,
    - Use registry numbers: `103-90-2` (CAS number), Reaxys (2208089)
    - Use PubMed ids: `11304127`

    """

    parameters = [
        AdvancedSearchAPIBAse.general_term,
        AdvancedSearchAPIBAse.page_number_param,
        AdvancedSearchAPIBAse.page_size_param,
    ]

    @extend_schema(parameters=parameters)
    def get(self, request):
        term = "*" if not request.query_params.get("term", "*").strip() else request.query_params.get("term", "*")
        term = term.strip()
        page_number = request.query_params.get("page", 1)
        page_size = request.query_params.get("size", 15)
        compounds = simple_get_compounds(term)

        if request.query_params.get("download", None):
            # If request contain download param, return an xlsx file
            return download_es_results(compounds)

        paginated_compounds = Paginator(compounds, page_size)
        response = ResponseAPI()
        try:
            results = list(paginated_compounds.page(page_number).object_list)
            response.results, response.total, response.number_pages = (
                results,
                paginated_compounds.count,
                paginated_compounds.num_pages,
            )
            return Response(response.model_dump(), status=HTTP_200_OK)
        except EmptyPage:
            response.total, response.number_pages = paginated_compounds.count, paginated_compounds.num_pages
            return Response(response.model_dump(), status=HTTP_204_NO_CONTENT)
        except InvalidPage:
            raise ValidationError({"detail": f"Error in page param, its value is: {page_number}"})


class AdvancedSearchAPI(AdvancedSearchAPIBAse):
    """
    ### Description
    This endpoint performs a searching of all the compounds using a set of specifications passed as a request body.
    We have seven kinds of different specifications:

    - OntologySpecification
    - FormulaSpecification
    - MassSpecification
    - MonoisotopicMassSpecification
    - ChargeSpecification
    - DatabaseNameSpecification
    - TextSearchSpecification

    Each of the above must have at least one operator: `AND`, `OR` and `BUT NOT`, represented by
    `and_specification`, `but_not_specification` and `or_specification`.

    An advanced search is performed when a combination of specifications and operators are created to retrieve a set
    of terms. With this endpoint, you can perform queries keeping the following structure:
     - `(A AND B AND C ... AND Z)`
     - `(A OR B OR C ... OR Z)`
     - `(A AND B AND C ... AND Y) BUT NOT Z`
     - `(A OR B OR C ... OR Y) BUT NOT Z`
    """

    @extend_schema(
        request=FullSpecification,
        examples=get_advanced_search_examples(),
        parameters=AdvancedSearchAPIBAse.base_query_params,
    )
    def post(self, request):
        try:
            full_specification = FullSpecification(**request.data)
            return self.resolve_request_full_specification(full_specification)
        except ValueError as e:
            raise ValidationError({"detail": e})


class StructureSearchAPI(AdvancedSearchAPIBAse):
    smiles_param = OpenApiParameter(
        name="smiles", type=str, description="Molecule structure in smiles representation.", required=True
    )
    search_type_param = OpenApiParameter(
        name="search_type",
        type=str,
        description=f"Structure search type. Options: {StructureSearch.SearchType}.",
        required=True,
        enum=StructureSearch.SearchType,
    )
    similarity_param = OpenApiParameter(
        name="similarity",
        type=float,
        description="Similarity percentage for similarity search (>=0.4 and <= 1.0)",
        required=False,
    )
    get_req_params = [
        smiles_param,
        search_type_param,
        similarity_param,
    ] + AdvancedSearchAPIBAse.base_query_params_no_has_struct.copy()

    @extend_schema(parameters=get_req_params)
    def get(self, request):
        smiles = self.request.query_params.get("smiles", None)
        search_type = self.request.query_params.get("search_type", None)
        similarity = self.request.query_params.get("similarity", None)
        if similarity is not None:
            similarity = float(similarity)
        structure_search = FullSpecification(
            structure_search=StructureSearch(structure=smiles, type=search_type, similarity=similarity)
        )
        return self.resolve_request_full_specification(structure_search)

    @extend_schema(request=StructureSearch, parameters=AdvancedSearchAPIBAse.base_query_params_no_has_struct)
    def post(self, request):
        structure_search = FullSpecification(structure_search=StructureSearch(**request.data))
        return self.resolve_request_full_specification(structure_search)


class OntologySearchAPI(AdvancedSearchAPIBAse):
    relation_param = OpenApiParameter(
        name="relation",
        type=str,
        description="Ontology relation.",
        required=True,
        enum=RelationType.get_chebi_ontology_relations(),
    )
    entity_param = OpenApiParameter(
        name="entity",
        type=str,
        description="ChEBI Identifier of the entity for which all the children in path will be found.",
        required=True,
    )
    get_req_params = [relation_param, entity_param] + AdvancedSearchAPIBAse.base_query_params.copy()

    @extend_schema(parameters=get_req_params)
    def get(self, request):
        """
        ### Description
        This endpoint performs a searching of all the compounds in the ontology,
        having into account an ontological relation and a ChEBI ID.Some examples of queries can be:

        **• Get all compounds that have a role "plan metabolite":** In this case, you should set the parameter relation
        to `"has_role"` and the parameter entity to `"CHEBI:76924"`, the chebi id for plant metabolite.

        **• Get all compounds that are "alcohols":** In this case, you should set the parameter relation to `"is_a"` and
        the parameter entity to `"CHEBI:30879"`, the chebi id for alcohol.

        """
        relation = self.request.query_params.get("relation", None)
        entity = self.request.query_params.get("entity", None)
        ont_req = Ontology(relation=relation, entity=entity)
        ont_spec = FullSpecification(ontology_specification=OntologySpecification(and_specification=[ont_req]))
        return self.resolve_request_full_specification(ont_spec)

    @extend_schema(request=Ontology, parameters=AdvancedSearchAPIBAse.base_query_params)
    def post(self, request):
        """
        ### Description
        This endpoint performs a searching of all the compounds in the ontology,
        having into account an ontological relation and a ChEBI ID.
        """
        ont_req = Ontology(**request.data)
        ont_spec = FullSpecification(ontology_specification=OntologySpecification(and_specification=[ont_req]))
        return self.resolve_request_full_specification(ont_spec)
