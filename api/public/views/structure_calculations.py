import logging

from drf_spectacular.utils import extend_schema, OpenApiParameter
from drf_spectacular.types import OpenApiTypes
from libRDChEBI.descriptors import (
    get_molformula,
    get_net_charge,
    get_avg_mass,
    get_monoisotopic_mass,
    get_mass_from_formula,
)
from libRDChEBI.depiction import depict_indigo
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.exceptions import ValidationError
from base.custom_django_parsers import PlainTextParser
from base.custom_django_renderers import PlainTextRenderer, PNGRenderer
from utils import str_to_bool
import abc
import traceback


class BaseStructureCalculationAPI(APIView):
    parser_classes = [PlainTextParser]
    renderer_classes = [PlainTextRenderer, BrowsableAPIRenderer]

    @abc.abstractmethod
    def run_calculation(self, molfile_str: str):
        raise NotImplementedError

    @extend_schema(
        request=OpenApiTypes.STR,
        responses={
            (200, "text/plain;charset=UTF-8"): OpenApiTypes.STR,
            (400, "text/plain;charset=UTF-8"): OpenApiTypes.STR,
        },
    )
    def post(self, request):
        try:
            calculation_result = self.run_calculation(request.data)
            return Response(calculation_result, status=HTTP_200_OK)
        except Exception:
            return Response("Failed to parse Molecule from UTF-8 MOLFILE or Formula.", status=HTTP_400_BAD_REQUEST)


class MolFormulaAPI(BaseStructureCalculationAPI):
    def run_calculation(self, molfile_str: str) -> str:
        molformula = get_molformula(molfile_str)
        if molformula is None:
            raise Exception("Could not calculate the molecule formula for the MOLFILE")
        return str(molformula)


class NetChargeAPI(BaseStructureCalculationAPI):
    def run_calculation(self, molfile_str: str) -> str:
        charge = get_net_charge(molfile_str)
        if charge is None:
            raise Exception("Could not calculate the net charge for the MOLFILE")
        return str(charge)


class AvgMassAPI(BaseStructureCalculationAPI):
    def run_calculation(self, molfile_str: str) -> str:
        mass = get_avg_mass(molfile_str)
        if mass == 0 or mass is None:
            raise Exception("Could not calculate the mass for the MOLFILE")
        return str(mass)


class MonoisotopicMassAPI(BaseStructureCalculationAPI):
    def run_calculation(self, molfile_str: str) -> str:
        mass = get_monoisotopic_mass(molfile_str)
        if mass == 0 or mass is None:
            raise Exception("Could not calculate the mass for the MOLFILE")
        return str(mass)


class AvgMassFromFormulaAPI(BaseStructureCalculationAPI):
    def run_calculation(self, formula_str: str) -> str:
        mass = get_mass_from_formula(formula_str, average=True)
        if mass == 0 or mass is None:
            raise Exception("Could not calculate the mass for Formula")
        return str(mass)


class MonoisotopicMassFromFormulaAPI(BaseStructureCalculationAPI):
    def run_calculation(self, formula_str: str) -> str:
        mass = get_mass_from_formula(formula_str, average=False)
        if mass == 0 or mass is None:
            raise Exception("Could not calculate the mass for Formula")
        return str(mass)


class DepictIndigoAPI(APIView):
    parser_classes = [PlainTextParser]
    renderer_classes = [PNGRenderer, BrowsableAPIRenderer]

    height_param = OpenApiParameter(
        name="height",
        type=int,
        description="Height in pixels of the generated image.",
        default=300,
        required=False,
    )
    width_param = OpenApiParameter(
        name="width",
        type=int,
        description="Width in pixels of the generated image.",
        default=300,
        required=False,
    )
    transbg_param = OpenApiParameter(
        name="transbg",
        type=bool,
        description="Use transparent background.",
        default=False,
        required=False,
    )

    def finalize_response(self, request, response, *args, **kwargs):
        if response.status_code == 400:
            self.renderer_classes = [PlainTextRenderer, BrowsableAPIRenderer]
            if (
                request.accepted_media_type == PNGRenderer.media_type
                or request.accepted_renderer.format == PNGRenderer.format
            ):
                request.accepted_renderer = PlainTextRenderer
                request.accepted_media_type = PlainTextRenderer.media_type
        return super().finalize_response(request, response, *args, **kwargs)

    @extend_schema(
        request=OpenApiTypes.STR,
        parameters=[width_param, height_param, transbg_param],
        responses={
            (200, PNGRenderer.media_type): OpenApiTypes.BINARY,
            (400, PlainTextRenderer.media_type): OpenApiTypes.STR,
        },
    )
    def post(self, request):
        height_str = self.request.query_params.get("height", "300")
        width_str = self.request.query_params.get("width", "300")
        transbg = str_to_bool(self.request.query_params.get("transbg", "false"))
        try:
            height = int(height_str)
            width = int(width_str)
        except Exception:
            raise ValidationError("Failed to interpret height or width as numbers.")

        if height < 50 or width < 50:
            raise ValidationError("Height or Width are too small. 50px is the minimum.")

        if height > 2000 or width > 2000:
            raise ValidationError("Height or Width are too big. 1000px is the maximum.")
        try:
            logging.info(f"PNG REQUEST WITH H: {height}, W: {width}, TRANS: {transbg}")
            image_png = depict_indigo(request.data, height=height, width=width, transbg=transbg)
        except Exception as e:
            traceback.print_exception(e)
            raise ValidationError("Failed to generate depiction from molfile.")
        if image_png is None:
            logging.error(f"INDIGO DEPICTION RETURN NONE FOR:\n{request.data}")
            raise ValidationError("Failed to generate depiction from molfile.")
        return Response(image_png, status=HTTP_200_OK)
