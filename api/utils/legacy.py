# Contains legacy code copied over from old chebi


def get_component_taxonomy_url(component_accession):
    return "https://purl.obolibrary.org/obo/" + component_accession.replace(":", "_")
