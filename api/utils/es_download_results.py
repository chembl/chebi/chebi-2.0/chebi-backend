from django.http import HttpResponse


def download_es_results(compounds):
    # Selective keys to include in the TSV file
    selected_keys = [
        "chebi_accession",
        "name",
        "ascii_name",
        "stars",
        "formula",
        "charge",
        "mass",
        "monoisotopicmass",
        "inchikey",
        "inchi",
        "smiles",
        "definition",
    ]

    compounds_response = compounds.extra(size=20000).source(fields=selected_keys).execute()

    # Create a string buffer to store TSV content
    tsv_buffer = "\t".join(selected_keys) + "\n"

    # Serialize JSON data to TSV format
    for hit in compounds_response["hits"]["hits"]:
        obj = hit["_source"]
        tsv_row = [str(obj.get(key, "-")) for key in selected_keys]
        tsv_buffer += "\t".join(tsv_row) + "\n"

    response = HttpResponse(tsv_buffer, content_type="text/tab-separated-values")
    response["Content-Disposition"] = 'attachment; filename="data.tsv"'
    return response
