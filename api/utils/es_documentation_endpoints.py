from typing import NamedTuple

from drf_spectacular.utils import OpenApiExample

from api.public.tests.advanced_search.use_cases import ComplexSpecificationData, specification_test_cases
from pydantic_models.advanced_search import FullSpecification


class DocumentationExamples(NamedTuple):
    """Tuple to represent a special example on the sagger documentation to the /advanced_search endpoint"""

    # you can see test cases in api/public/tests/specification_tests_cases.py
    test_case_name: str
    summary: str
    description: str | None = None


star_note = "**NOTE**: You **must** set the `three_star_only` parameter to `False` (check the _Parameters_ section showed above), in order to get all the results entries. Otherwise, you will get 3 stars entries only"

advanced_search_documentation_examples = (
    DocumentationExamples(
        test_case_name="compounds_with_formula_C6H12O7_but_not_is_a_carbohydrates_and_carbohydrate_derivatives",
        summary="Find all compounds which are not carbohydrates and carbohydrate derivatives with formula C6H12O7",
        description="The above query can be interpreted as follow: Find all compounds with formula C6H12O7, BUT NOT (**IS A**) carbohydrates and carbohydrate derivatives **(CHEBI:78616)**.<br>"
        f"{star_note}",
    ),
    DocumentationExamples(
        test_case_name="is_a_phenol_but_not_has_role_plant_metabolite_and_mass_288_290",
        summary="Get all terms which are phenols with an average mass between 288 to 290, exclude those terms which do not have rol plant metabolite.",
        description="The above query can be interpreted as follow: **IS A** phenols **(CHEBI:33853)** AND Average Mass range from 288 to 290 BUT NOT (**HAS ROLE**) plant metabolite **(CHEBI:76924)**.<br>"
        f"{star_note}",
    ),
    DocumentationExamples(
        test_case_name="is_a_quinoles_and_is_a_phenols_but_not_is_a_antimalarial",
        summary="Terms which are quinolines and phenols, but not antimalarial",
        description="The above query can be interpreted as follow: **IS A** quinolines **(CHEBI:26513)** AND **IS A** phenols **(CHEBI:33853)** BUT NOT (**IS A**) antimalaria **(CHEBI:38068)**.<br>"
        f"{star_note}",
    ),
    DocumentationExamples(
        test_case_name="all_compounds_HMDB_database",
        summary="All terms which have as a reference, the HMDB database",
        description="The above query can be interpreted as follow: **IS A** quinolines **(CHEBI:26513)** AND **IS A** phenols **(CHEBI:33853)** BUT NOT (**IS A**) antimalaria **(CHEBI:38068)**.<br>"
        f"{star_note}",
    ),
    DocumentationExamples(
        test_case_name="simple_monoisotopicmass_specification",
        summary="All compounds which have a monoisotopic mass range from 44.5 to 70.5",
        description=f"{star_note}",
    ),
    DocumentationExamples(
        test_case_name="text_specification_3_stars_WILDCARD",
        summary="Get all terms with acid in its name, include only 3 stars terms",
    ),
    DocumentationExamples(
        test_case_name="simple_formula_specification",
        summary="Get compounds which have as a formula C10H7NO4",
        description=f"{star_note}",
    ),
    DocumentationExamples(
        test_case_name="advanced_text_search_BRAND_NAME",
        summary="Get compound with Sophoricol brand name",
        description="The body has two list: `categories` and `texts`. Item in `text[i]` will be searched against `categories[i]`.<br>"
        f"{star_note}",
    ),
    DocumentationExamples(
        test_case_name="advanced_text_or_but_not_and",
        summary="Get all stars terms with 'acid' or 'benzo' in their name, excluding any term with -1 charge.",
        description=f"{star_note}",
    ),
)


def get_advanced_search_examples() -> list[OpenApiExample]:
    """Add examples for advanced search on swagger documentation."""
    open_api_examples = []
    for example in advanced_search_documentation_examples:
        open_api_example = OpenApiExample(
            example.test_case_name,
            summary=example.summary,
            description=example.description,
        )
        for test_case in filter(lambda t: t.name == example.test_case_name, specification_test_cases):
            if isinstance(test_case, ComplexSpecificationData):
                specifications = test_case.specifications
                open_api_example.value = FullSpecification(
                    **{spec.specification_field: spec.specification for spec in specifications}
                ).model_dump(exclude_unset=True)
            else:
                body = {test_case.specification_field: test_case.specification}
                open_api_example.value = FullSpecification(**body).model_dump(exclude_unset=True)

            open_api_examples.append(open_api_example)

    return open_api_examples
