from base.es_settings import MAPPINGS


def get_mappings_by_field(field: str = "all") -> dict:
    """Return the elasticsearch configuration for that specific field, for example, if category = 'chebi_accesion',
    then it returns the mapping configuration:
    {
          "type": "keyword",
          "fields": {
            "identification": {
              "type": "text",
              "analyzer": "keyword_alphanumeric_without_space_lowercase_analyzer"
            },
            "lower": {
              "type": "keyword",
              "normalizer": "lowercase"
            },
            "special": {
              "type": "keyword",
              "normalizer": "alphanumeric_without_space_lowercase_normalizer"
            }
          }
    }
    """
    mappings_information: dict = list(MAPPINGS.values())[0]
    if field == "all":
        return mappings_information
    return mappings_information["mappings"]["properties"].get(field)


def get_field_names(_type: str = "all") -> list[str]:
    """Return a list with the field names in the elasticsearch index"""
    mappings_information: dict = list(MAPPINGS.values())[0]
    if _type == "all":
        return mappings_information["mappings"]["properties"].keys()
    else:
        return [
            field_name
            for field_name, info in mappings_information["mappings"]["properties"].items()
            if info["type"] == _type
        ]
