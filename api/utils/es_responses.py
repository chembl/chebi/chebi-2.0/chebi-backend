"""
Using the elasticsearch_dsl library, we can have multiple ways of returning data. The default response of elasticsearch
dsl is a big dictionary with information about shards, analyzers, scores and other stuff which can be cumbersome, classes
here should be used in .response_class() method. You can think that it is a kind of simple serializer

This issue on Github can clarify the idea: https://github.com/elastic/elasticsearch-dsl-py/issues/1041
"""

from collections import UserDict
from collections.abc import Iterable


class SimpleResponse(UserDict):
    """SimpleResponse is a class that must be used in .response_class method, you will get only the data in
    self['hits']['hits'] which does not contain elasticsearch metadata (number of shards, analyzers, etc.).
    Take a look the inner '_source' dictionary in the elasticsearch response, there you will see all the fields related to the
    compounds. If you want to select a couple of fields, you must use .extra(_source=["field_1", "field_2"]) method"""

    def __init__(self, search, response):
        self.search = search
        self.response = response
        super().__init__(**response)

    def __iter__(self):
        yield from self["hits"]["hits"]


class SimpleResponseFilter(UserDict):
    """This class defines the process to filter the results returned by an elasticsearch response.
    You must inherit from this class and set the property "fields", which is a list of properties you want to return.
    Only subclasses of SimpleResponseFilter are used in elasticsearch_dsl .response_class() method. You can think of
    each subclass as a sort of Django Serializer. It is not necessary to use .extra(_source=["field_1", "field_2"])
    method, instead of it, create a subclass and set the "fields" parameter"""

    def __init__(self, search, response):
        self.search = search
        self.response = response
        super().__init__(**response)

    def filter_data(self, _source) -> Iterable:
        """Filter the properties to show in the final response based on the fields property"""
        sources = {}  # Here we are gonna store the fields we want to show based on the fields list
        for k, v in _source.items():
            if k in self.fields:
                sources[k] = v
        return sources

    def __iter__(self):
        hits = self["hits"]["hits"]
        for hit in hits:
            hit["_source"] = self.filter_data(hit["_source"])
        yield from hits

    @property
    def fields(self) -> Iterable:
        """Fields you want to return in an elasticsearch response (available in '_source' dictionary)"""
        raise NotImplementedError("Subclasses must implement the 'fields' property")


class MinimalCompoundResults(SimpleResponseFilter):
    fields = [
        "id",
        "default_structure",
        "structures",
        "chebi_accession",
        "name",
        "ascii_name",
        "stars",
        "formula",
        "charge",
        "mass",
        "monoisotopicmass",
        "definition",
        "inchikey",
        "inchi",
        "smiles",
    ]
