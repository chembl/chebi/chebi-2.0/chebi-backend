import os
import time
import pickle
import requests
from django.core.exceptions import ImproperlyConfigured
from rdkit import Chem
from rdkit.Chem import rdSubstructLibrary
from FPSim2 import FPSim2Engine


def download_file(file_url: str) -> str:
    file_path = file_url.split("/")[-1]
    if not os.path.exists(file_path):
        # NOTE the stream=True parameter below
        with requests.get(file_url, stream=True) as r:
            r.raise_for_status()
            with open(file_path, "wb") as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
    return file_path


# ----------------------------------------------------------------------------------------------------------------------
# SUBSTRUCTURE
# ----------------------------------------------------------------------------------------------------------------------
SSS_RDKIT_FILE_URL = os.environ.get("SSS_RDKIT_FILE_URL")
SSS_RDKIT_LIB: rdSubstructLibrary.SubstructLibrary = None


def get_rdkit_slib() -> rdSubstructLibrary.SubstructLibrary:
    global SSS_RDKIT_FILE_URL, SSS_RDKIT_LIB
    if not SSS_RDKIT_LIB and SSS_RDKIT_FILE_URL:
        t_ini = time.time()
        sss_file_path = download_file(SSS_RDKIT_FILE_URL)
        print("LOADING RDKIT SSSLIB . . .")
        with open(sss_file_path, "rb") as inf:
            SSS_RDKIT_LIB = pickle.load(inf)
            print("RDKIT SUBSTRUCTURE FILE LOADED IN {0} SECS".format(time.time() - t_ini))
    return SSS_RDKIT_LIB


def get_substructure_chebi_ids(mol_query: Chem.Mol):
    slib = get_rdkit_slib()
    if not slib:
        raise ImproperlyConfigured("Could not setup a SSS LIB to be used for substructure search.")
    mol_matches = slib.GetMatches(mol_query, maxResults=1000, numThreads=4)
    chebi_ids = [slib.GetKeyHolder().GetKey(match_i) for match_i in mol_matches]
    return chebi_ids


# ----------------------------------------------------------------------------------------------------------------------
# SIMILARITY
# ----------------------------------------------------------------------------------------------------------------------
FPSIM2_FILE_URL = os.environ.get("FPSIM2_FILE_URL")
FPSIM_ENGINE: FPSim2Engine = None


def get_fpsim_engine() -> FPSim2Engine:
    global FPSIM_ENGINE, FPSIM2_FILE_URL
    if not FPSIM_ENGINE and FPSIM2_FILE_URL:
        t_ini = time.time()
        fpsim2_file_path = download_file(FPSIM2_FILE_URL)
        FPSIM_ENGINE = FPSim2Engine(fpsim2_file_path)
        print("FPSIM2 FILE LOADED IN {0} SECS".format(time.time() - t_ini))
    return FPSIM_ENGINE


def get_similar_chebi_ids(mol_query: Chem.Mol, similarity: float = 0.7):
    """
    :param mol_query: the rdkit molecule used for querying
    :param similarity: the minimum similarity threshold
    :return: a list with tuples of (molregno, similarity)
    """
    query_string_std = Chem.MolToMolBlock(mol_query)
    if similarity < 0.4 or similarity > 1:
        raise ValueError("Similarity should have a value between 0.4 and 1.")
    fpsim2_engine = get_fpsim_engine()
    if not fpsim2_engine:
        raise ImproperlyConfigured("Could not setup a FPSIM2 ENGINE to be used for similarity search.")
    return fpsim2_engine.similarity(query_string_std, similarity, n_workers=1)
