from elasticsearch_dsl.query import SF, FunctionScore


def star_function_score(query):
    """
    This function_score multiply a document's calculated score by its number of star. So, we are going to have in upper
    places results with 3 stars. This is useful to order the results in a response.
    See more here: https://www.elastic.co/guide/en/elasticsearch/reference/7.15/modules-scripting-fields.html#scripting-score
    """
    script_score = {"lang": "expression", "source": "_score * doc['stars.integer']"}
    function_score = FunctionScore(
        query=query, boost_mode="multiply", functions=[SF("script_score", script=script_score)]
    )
    return function_score
