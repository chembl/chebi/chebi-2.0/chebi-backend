def is_unique_chebi_name(name: str):
    """
    Validate if the provided  ChEBI name already exists
    If it exists, return the Compound object that contains it
    If it doesn't, return None
    """
    pass
