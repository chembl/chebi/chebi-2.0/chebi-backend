from django.db import models


# TODO: Remove Space from db value of these name to make safe for URL encoding (used by HTMX)
class NameTypeChoices(models.TextChoices):
    """
    This class lists all possible options for model field
    """

    SYNONYM = "SYNONYM", "Synonym"
    INN = "INN", "INN"
    BRAND_NAME = "BRAND NAME", "Brand Name"
    UNIPROT_NAME = "UNIPROT NAME", "UniProt Name"
    IUPAC_NAME = "IUPAC NAME", "IUPAC Name"


class NameTypeSubmitterFormChoices(models.TextChoices):
    """
    This choices class is used for external submitters, don't show UniProt name here
    """

    __empty__ = "---------"
    SYNONYM = "SYNONYM", "Synonym"
    INN = "INN", "INN"
    BRAND_NAME = "BRAND NAME", "Brand Name"
    IUPAC_NAME = "IUPAC NAME", "IUPAC Name"


class LanguageChoices(models.TextChoices):
    ENGLISH = "en", "English"
    FRENCH = "fr", "French"
    LATIN = "la", "Latin"
    GERMAN = "de", "German"
    SPANISH = "es", "Spanish"


class DatabaseAccessionTypeChoices(models.TextChoices):
    __empty__ = "---------"
    MANUAL_X_REF = "MANUAL_X_REF", "Manual Cross Reference"
    REGISTRY_NUMBER = "REGISTRY_NUMBER", "Registry Number"
    CITATION = "CITATION", "Citation"
    CAS = "CAS", "CAS Number"
