from functools import lru_cache
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Q
from django.utils.functional import cached_property
from libRDChEBI.descriptors import has_r_group

from api.utils.legacy import get_component_taxonomy_url
from chebi_models.models import User
from chebi_models.models.choices import LanguageChoices, NameTypeChoices, DatabaseAccessionTypeChoices
from utils import capture_manual_exception


class BaseModel(models.Model):
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    # TODO: Remove Null True from created_by
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name="created_%(class)s", null=True)
    modified_on = models.DateTimeField(auto_now=True, null=True)
    modified_by = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name="modified_%(class)s")
    deleted_on = models.DateTimeField(null=True)
    deleted_by = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name="deleted_%(class)s")

    class Meta:
        abstract = True


class Status(models.Model):
    code = models.CharField(max_length=1)
    name = models.CharField(max_length=32)

    def __str__(self):
        return f"Status - {self.code} - {self.name}"

    class Meta:
        db_table = "status"
        verbose_name_plural = "Status"


class Source(BaseModel):
    name = models.CharField(max_length=60)
    url = models.URLField(max_length=600, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    prefix = models.CharField(max_length=256)
    has_cas_url = models.BooleanField(
        default=False, help_text="Optional and only relevant for sources that provide " "CAS number"
    )
    is_active = models.BooleanField(
        default=True,
        help_text="If unchecked, this source become inactive and will not appear in submitter tool. "
        "Existing data won't be impacted by this",
    )
    is_synonym_source = models.BooleanField(default=False)
    is_brand_name_source = models.BooleanField(default=False)
    is_compound_origin_source = models.BooleanField(default=False)
    is_species_accession_source = models.BooleanField(default=False)
    is_manual_xref_source = models.BooleanField(default=False)
    is_automatic_xref_source = models.BooleanField(default=False)
    is_registry_number_source = models.BooleanField(default=False)
    is_citation_source = models.BooleanField(default=False)
    is_inn_source = models.BooleanField(default=False)
    is_iupac_source = models.BooleanField(default=False)
    is_cas_source = models.BooleanField(default=False)

    class Meta:
        db_table = "source"

    def __str__(self):
        return self.name


class Compound(BaseModel):
    name = models.TextField()
    # TODO: Status field can be removed from Compound table and let curator directly change stars
    # Status was used to perform checks when changing from submitted to checked
    # Status differentiates between future release date entries and other 0 star entries
    # but now there is_released field to differentiate this
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name="+", null=True)
    # Source is important for bulk submissions and audit purposes, for curator to see where this data was loaded from
    # Keep it for now, and display it in curator tool (By default source to be Submitter)
    source = models.CharField(max_length=512, default="SUBMITTER")
    parent = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, related_name="children")
    # TODO: Do we need this field? Probably when merging/de-merging?
    merge_type = models.CharField(max_length=256, null=True)
    chebi_accession = models.CharField(max_length=25)
    definition = models.TextField(null=True, blank=True, help_text="This will be publicly visible")
    ascii_name = models.TextField(null=True, blank=True)
    # TODO: Default stars = 2 if external submitter submitted entry. it should be 1 if curator submitted it
    stars = models.PositiveSmallIntegerField(validators=[MaxValueValidator(3), MinValueValidator(1)], default=2)
    release_date = models.DateField(
        null=True,
        blank=True,
        help_text="Optional: Provide a future release date if you don't want this compound to be released publicly now.",
    )
    submitter_notes = models.TextField(
        null=True, blank=True, help_text="Comments about this submission, only visible to curator."
    )
    is_released = models.BooleanField(default=True)

    class Meta:
        db_table = "compounds"

    @cached_property
    def secondary_ids(self):
        # Returns list of Chebi_accessions ['CHEBI:123', 'CHEBI:435']
        return Compound.objects.filter(parent=self).values_list("chebi_accession", flat=True)

    # To be used wherever we want to filter data for compound and its children
    @cached_property
    def compound_with_children_ids(self) -> list:
        """
        Finds the parent of given entry (if it is secondary ID)
        Returns a list of all secondary IDs along with parent itself
        """
        parent_compound = self.parent if self.parent else self
        return list(Compound.objects.filter(parent=parent_compound).values_list("pk", flat=True)) + [parent_compound.pk]

    @cached_property
    def default_structure(self):
        qs = Structure.public_objects_qs(stars=self.stars).filter(compound_id__in=self.compound_with_children_ids)
        default_structure = qs.filter(default_structure=True).first()
        if default_structure:
            # Return default structure if exists
            return default_structure
        if qs.exists():
            # Fallback, if structure exists but there's no default set, then return first structure and log error
            capture_manual_exception(
                message=f"CHEBI:{self.pk} No default structure found, returning the first structure"
            )
            return qs.first()

    @cached_property
    def chemical_data(self):
        if self.default_structure:
            return ChemicalData.objects.filter(compound_id=self.default_structure.compound_id).first()
        return ChemicalData.objects.filter(compound=self).first()

    @staticmethod
    def pre_check_existing_name(name):
        """Checks if the provided ChEBI name already exists"""
        # Ideally there should not be more than one results, but since we do not have a unique constraint on the
        # database we might get multiple results. In that case we will return first one
        # TODO: How to handle XML tags here?
        return (
            Compound.objects.filter(Q(name__iexact=name) | Q(ascii_name__iexact=name))
            .only("pk", "chebi_accession")
            .first()
        )


class CompoundOrigins(BaseModel):
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name="compound_origins")
    species_text = models.CharField("Species Name", max_length=1024, null=True)
    species_accession = models.CharField(max_length=30, null=True)
    species_source = models.ForeignKey(Source, on_delete=models.PROTECT, related_name="species_origins", null=True)
    component_source = models.ForeignKey(Source, on_delete=models.PROTECT, related_name="component_origins", null=True)
    component_text = models.CharField("Component Name", max_length=1024, null=True)
    component_accession = models.CharField(max_length=30, null=True)
    strain_text = models.CharField(max_length=1024, null=True, blank=True)
    strain_accession = models.CharField(max_length=30, null=True, blank=True)
    strain_source = models.ForeignKey(
        Source, on_delete=models.PROTECT, related_name="compound_origins_strains", blank=True, null=True
    )
    source = models.ForeignKey(Source, on_delete=models.PROTECT)
    source_accession = models.CharField(max_length=1024, null=True)
    comments = models.TextField(null=True, blank=True)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name="+")

    @classmethod
    def public_objects_qs(cls, stars=None):
        if stars and stars >= 3:
            return cls.objects.filter(status__code__in=["S", "C"])
        return cls.objects.filter(status__code__in=["S", "C", "E"])

    class Meta:
        db_table = "compound_origins"
        verbose_name_plural = "Compound Origins"

    @property
    def species_accession_url(self):
        return (
            self.species_source.url.replace("*", self.species_accession)
            if self.species_accession and self.species_source
            else None
        )

    @property
    def component_accession_url(self):
        return get_component_taxonomy_url(self.component_accession)


class DatabaseAccession(BaseModel):
    compound = models.ForeignKey(Compound, related_name="database_accessions", on_delete=models.CASCADE)
    accession_number = models.CharField(max_length=255)
    type = models.CharField(max_length=25, db_index=True, choices=DatabaseAccessionTypeChoices.choices)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name="+")
    # TODO: Remove null=True from all source FKs
    source = models.ForeignKey(Source, on_delete=models.DO_NOTHING, null=True)

    class Meta:
        db_table = "database_accession"

    @classmethod
    def public_objects_qs(cls, stars=None):
        """
        A generic class method that returns queryset of public objects.
        If it is a 3-star entry only shows Submitted and Checked Status
        Otherwise by default, shows OK status as well along with above two
        """
        if stars and stars >= 3:
            return cls.objects.filter(status__code__in=["S", "C"])
        return cls.objects.filter(status__code__in=["S", "C", "E"])


class Names(BaseModel):
    compound = models.ForeignKey(Compound, related_name="names", on_delete=models.CASCADE)
    name = models.TextField()
    type = models.CharField(max_length=25, choices=NameTypeChoices)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name="+")
    source = models.ForeignKey(Source, on_delete=models.DO_NOTHING, null=True)
    adapted = models.BooleanField(default=False)
    language_code = models.CharField(
        verbose_name="Language", max_length=20, default=LanguageChoices.ENGLISH, choices=LanguageChoices
    )
    ascii_name = models.TextField()

    @classmethod
    def public_objects_qs(cls, stars=None):
        """
        A generic class method that returns queryset of public names.
        If it is a 3-star entry only shows Submitted, Checked and Deprecated Status
        Otherwise by default, shows OK status as well along with above three
        """
        if stars and stars >= 3:
            return cls.objects.filter(status__code__in=["S", "C", "P"])
        return cls.objects.filter(status__code__in=["S", "C", "P", "E"])

    class Meta:
        db_table = "names"
        verbose_name_plural = "Names"

    @staticmethod
    def pre_check_similar_names(name):
        """
        Checks for any existing records matching the name provided
        Output from this method is used to suggest similar entries when a new entry is being created
        """
        # Exclude UniProt Name from similarity
        return (
            Names.public_objects_qs()
            .filter(Q(name__iexact=name) | Q(ascii_name__iexact=name))
            .exclude(type=NameTypeChoices.UNIPROT_NAME.value)
        )


class Reference(BaseModel):
    compound = models.ForeignKey(Compound, related_name="references", on_delete=models.CASCADE)
    reference_id = models.CharField(max_length=60)
    location_in_ref = models.CharField(max_length=90, null=True)
    autogenerated = models.BooleanField(null=True)
    source = models.ForeignKey(Source, on_delete=models.DO_NOTHING, null=True)
    accession_number = models.CharField(max_length=60)
    reference_name = models.CharField(max_length=1024, null=True)

    class Meta:
        db_table = "reference"


class RelationType(BaseModel):
    code = models.CharField(max_length=60, unique=True)
    allow_cycles = models.BooleanField()
    description = models.CharField(max_length=200)

    class Meta:
        db_table = "relation_type"

    @staticmethod
    @lru_cache
    def get_chebi_ontology_relations_labeled():
        rel_types = RelationType.objects.all()
        chebi_ont_relations = []
        for rel_type_i in rel_types:
            chebi_ont_relations.append({"label": rel_type_i.description, "relation": rel_type_i.code})
        return chebi_ont_relations

    @staticmethod
    @lru_cache
    def get_chebi_ontology_relations():
        labeled_relations = RelationType.get_chebi_ontology_relations_labeled()
        return [rel_i["relation"] for rel_i in labeled_relations]


class Relation(BaseModel):
    relation_type = models.ForeignKey(RelationType, on_delete=models.DO_NOTHING)
    init = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name="outgoing_relations")
    final = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name="incoming_relations")
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    evidence_accession = models.CharField(max_length=255, null=True)
    evidence_source = models.ForeignKey(
        Source, on_delete=models.PROTECT, related_name="relation_sources", blank=True, null=True
    )

    class Meta:
        db_table = "relation"


class Structure(BaseModel):
    compound = models.ForeignKey(Compound, related_name="structures", on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    molfile = models.TextField(null=True)
    smiles = models.TextField(null=True)
    standard_inchi = models.TextField(null=True)
    standard_inchi_key = models.TextField(null=True)
    # TODO: Drop dimensions field, only store 2D Structures. Convert existing 3D to 2D
    # /chebi-etl/-/issues/54
    dimension = models.CharField(max_length=25, default="2D")
    default_structure = models.BooleanField()

    @classmethod
    def public_objects_qs(cls, stars=None):
        """
        A generic class method that returns queryset of public objects.
        If it is a structure of 3-star entry only shows Submitted anc Checked Status
        For all other entries, shows OK status as well along with Submitted and Checked
        """
        if stars and stars >= 3:
            return cls.objects.filter(status__code__in=["S", "C"])
        return cls.objects.filter(status__code__in=["S", "C", "E"])

    @property
    def is_r_group(self):
        """Check R group based on molfile"""
        return has_r_group(self.molfile)

    class Meta:
        db_table = "structures"


class StructureRegistry(models.Model):
    structure = models.ForeignKey(Structure, related_name="layers", on_delete=models.CASCADE)
    layers = models.JSONField(null=True)
    hash = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "structure_registry"
        verbose_name_plural = "Structure Registry"


class Comments(BaseModel):
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name="comments")
    comment = models.TextField(null=True)  # remove null True after checking later
    author_name = models.CharField(max_length=100, null=True)
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    datatype = models.CharField(max_length=50)
    # What does data_type_id mean?
    datatype_id = models.IntegerField()

    class Meta:
        db_table = "comments"
        verbose_name_plural = "Comments"


class ChemicalData(BaseModel):
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE)
    # Check if the status column could be removed later on
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    # TODO: Verify these field limits
    formula = models.TextField(null=True, blank=True)
    charge = models.SmallIntegerField(null=True, blank=True)
    mass = models.DecimalField("Average Mass", max_digits=10, decimal_places=3, null=True, blank=True)
    monoisotopic_mass = models.DecimalField("Monoisotopic Mass", max_digits=12, decimal_places=5, null=True, blank=True)
    is_autogenerated = models.BooleanField(default=False)
    # Can this be 1-1 relation in future?
    structure = models.ForeignKey(Structure, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        db_table = "chemical_data"
        verbose_name_plural = "Chemical Data"


class Wurcs(BaseModel):
    # TODO: Using structure as PK for now, need to revisit this decision
    structure = models.OneToOneField(
        Structure,
        on_delete=models.CASCADE,
        related_name="wurcs",
        primary_key=True,
    )
    wurcs = models.TextField()

    class Meta:
        db_table = "wurcs"
        verbose_name_plural = "Wurcs"
