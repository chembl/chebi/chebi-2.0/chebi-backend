from django.db.models.signals import post_save
from django.dispatch import receiver

from chebi_models.models import Compound


@receiver(post_save, sender=Compound)
def assign_chebi_accession(sender, instance, created, **kwargs):
    if created:
        # Assign a chebi accession after instance is created
        # TODO: Need to assign default Ontology here
        instance.chebi_accession = f"CHEBI:{instance.pk}"
        instance.save(update_fields=["chebi_accession"])
