from django.apps import apps
from django.contrib import admin

# Register all models for now
for model in apps.get_app_config("chebi_models").get_models():
    admin.site.register(model)
