from functools import lru_cache
from typing import Self, Optional

from pydantic import BaseModel, field_validator, Field
from rdkit import Chem
from rdkit.Chem import inchi
import enum


@lru_cache
def rdkit_mol_from_smiles_or_molblock(structure: str) -> Optional[Chem.Mol]:
    """
    converts a string SMILES or MOLFILE into an RDKIT Molecule
    """
    if structure is None:
        return None
    # if the input has linefeed it is a molfile
    elif "\n" in structure:
        print('TRYING TO PARSE MOLFILE')
        return Chem.MolFromMolBlock(structure)
    # otherwise it will be treated as smiles
    else:
        structure = structure.strip()
        return Chem.MolFromSmiles(structure)


class StructureSearch(BaseModel, validate_assignment=True):
    class SearchType(enum.StrEnum):
        CONNECTIVITY = enum.auto()
        SIMILARITY = enum.auto()
        SUBSTRUCTURE = enum.auto()

    structure: Optional[str] = Field(
        None,
        description='Structure that should be used for searching in SMILES or MOLFILE/SDF format.',
        examples=['c1ccccc1', 'CCO', 'Cn1cnc2c1c(=O)n(C)c(=O)n2C']
    )
    type: SearchType = SearchType.CONNECTIVITY
    similarity: Optional[float] = Field(
        None,
        ge=0.4,
        le=1.0,
        description="Similarity percentage for similarity search (>=0.4 and <= 1.0)",
        examples=[0.4, 0.69, 0.9, 1.0],
    )

    @classmethod
    @field_validator("structure")
    def check_structure(cls, structure) -> Self:
        if structure and "\n" not in structure:
            structure = structure.strip()
        rdkit_mol = rdkit_mol_from_smiles_or_molblock(structure)
        if not structure:
            assert rdkit_mol is None, "An empty SMILES/MOLFILE should not have a molecule representation"
        else:
            assert rdkit_mol is not None, f"Failed to obtain a molecule representation for:\n{structure}"
        return structure

    def get_rdkit_mol(self) -> Chem.Mol:
        return rdkit_mol_from_smiles_or_molblock(self.structure)

    def get_inchi_connectivity_layer(self) -> Optional[str]:
        full_inchi = inchi.MolToInchi(self.get_rdkit_mol())
        inchi_key_parts = inchi.InchiToInchiKey(full_inchi).split("-")
        return inchi_key_parts[0]
