from pydantic import BaseModel


class ResponseAPI(BaseModel):
    """Response structure used by the Elasticsearch endpoints"""

    results: list[dict] = []
    total: int = 0
    number_pages: int = 0
