import enum
from typing import Annotated, Literal, TypeAlias

from annotated_types import Len
from pydantic import BaseModel, StrictInt, field_validator, model_validator, StrictBool
from elasticsearch_dsl import Q as EQ

from api.utils.es_mappings import get_field_names, get_mappings_by_field
from api.utils.es_responses import SimpleResponse
from base.es_settings import ES_SEARCH
from pydantic_models.structure_search import StructureSearch

EmptyList = Annotated[list, Len(max_length=0)]
StarList = Annotated[list[StrictInt], Len(max_length=2, min_length=1)]
CategoriesText = tuple(list(get_field_names()) + ["all"])  # Allowed categories in Text search.


class Star(enum.IntEnum):
    THREE_STAR = 3
    TWO_STAR = 2


class Ontology(BaseModel):
    relation: str
    entity: str

    @property
    def id_(self) -> int:
        return int(self.entity.split(":")[1])

    @property
    def entities(self) -> list[int] | EmptyList:
        """Returns the children ids of an entity as an integer list. If we want to get, for example, all the compounds
        which has_role "metabolite", then we need to get in the first place, all the children of "metabolite", those
        entities will be returned in this function and used in the final ontological query. The above process is performed
        for all relations except "is_a", for this one is unnecessary."""
        if not (self.relation == "is_a"):
            es_query = EQ("term", chebi_accession=self.entity)
            response = ES_SEARCH.query(es_query).extra(_source=["children.id"]).response_class(SimpleResponse)
            results = response.execute()["hits"]["hits"]
            if results:
                children = results[0]["_source"]["children"]
                return list(child.get("id") for child in children)
            return list()
        return [self.id_]


class Term(BaseModel):
    term: str


class Range(BaseModel):
    init_range: float
    final_range: float


class Text(BaseModel):
    text: str | float
    # Category must be a valid elasticsearch field name, "all"
    category: Literal[CategoriesText]

    @property
    def field_mapping(self) -> dict:
        """Return the elasticsearch configuration for that specific field."""
        property_information = get_mappings_by_field(self.category)
        return property_information


class Nested(BaseModel):
    categories: list
    texts: list

    @property
    def nested_field(self) -> str:
        """Return elasticsearch nested field"""
        return self.categories[0].split(".")[0]

    @property
    def field_mapping(self) -> dict:
        """Return the elasticsearch configuration for that specific field."""
        property_information = get_mappings_by_field(self.nested_field)
        return property_information

    @property
    def is_all(self) -> bool:
        """Return True if client sends a Nested expression with .all operator"""
        return all("all" in category for category in self.categories) and len(self.categories) == 1

    @property
    def nested_sub_fields(self) -> list:
        """Get all nested subfields, if client sends <nested_field>.all, we return all the nested subfields present in elasticsearch
        For example, if client sends ["compound_origins.all], then we return ["comments", "component_accession", "component_text",
        "source_accession", "species_accession", "species_text", "strain_accession", "strain_text"].
        If client send a defined subfield, for example: ["compound_accession.source_accession"], then we return simply the defined
        subfield: ["source_accession"]
        """
        sub_fields = list(c.split(".")[1] for c in self.categories)
        if self.is_all:
            return list(self.field_mapping.get("properties", {}).keys())
        return sub_fields

    @model_validator(mode="before")
    @classmethod
    def check_information(cls, data: dict):
        if isinstance(data, dict):
            input_categories = data["categories"]
            input_texts = data["texts"]
            nested_field_names = get_field_names(_type="nested")

            assert any(
                (nested_field in category) for nested_field in nested_field_names for category in input_categories
            ), f"Categories field must include an elasticsearch nested filed: {', '.join(nested_field_names)}"

            assert len(input_texts) == len(input_categories), (
                "You only can search a text in only one specific field, if you want to search the term "
                "in other subfields, you can use .all"
            )

            assert all(
                "." in category for category in input_categories
            ), "You are using a Nested expression, you must use a dot (.) to indicate an inner elasticsearch field."

            assert (
                len(set(category.split(".")[0] for category in input_categories)) == 1
            ), "You must use the same nested elasticsearch field along a list. You provided: {input_categories}"

            if any("all" in category for category in input_categories):
                assert (
                    len(input_categories) == 1 and len(input_texts) == 1
                ), "You can use .all to perform a searching over one elasticsearch nested field only"

        return data


class OntologySpecification(BaseModel):
    and_specification: list[Ontology] = []
    or_specification: list[Ontology] = []
    but_not_specification: list[Ontology] = []


class TermSpecification(BaseModel):
    and_specification: list[Term] = []
    or_specification: list[Term] = []
    but_not_specification: list[Term] = []


class RangeSpecification(BaseModel):
    and_specification: list[Range] = []
    or_specification: list[Range] = []
    but_not_specification: list[Range] = []


class TextSearchSpecification(BaseModel):
    and_specification: list[Text | Nested] = []
    or_specification: list[Text | Nested] = []
    but_not_specification: list[Text | Nested] = []

    @field_validator("and_specification", "or_specification", "but_not_specification", mode="before")
    @classmethod
    def create_correct_model(cls, specification: list[dict] | BaseModel):
        """Text search specification can have Nested or Text models, here we are validating the dictionary keys to
        decide which model to instantiate: Nested or Text"""
        for spec in specification:
            if isinstance(spec, BaseModel):
                spec = spec.model_dump()

            if "categories" in spec and "texts" in spec:
                yield Nested(**spec)
            elif "category" in spec and "text" in spec:
                yield Text(**spec)
            else:
                raise ValueError("Invalid specification type")


MassSpecification: TypeAlias = RangeSpecification
MonoisotopicMassSpecification: TypeAlias = RangeSpecification
ChargeMassSpecification: TypeAlias = RangeSpecification
FormulaSpecification: TypeAlias = TermSpecification
DatabaseNameSpecification: TypeAlias = TermSpecification


class FullSpecification(BaseModel):
    structure_search: StructureSearch | None = None
    ontology_specification: OntologySpecification | EmptyList = []
    formula_specification: FormulaSpecification | EmptyList = []
    mass_specification: MassSpecification | EmptyList = []
    monoisotopicmass_specification: MonoisotopicMassSpecification | EmptyList = []
    charge_specification: ChargeMassSpecification | EmptyList = []
    database_name_specification: DatabaseNameSpecification | EmptyList = []
    text_search_specification: TextSearchSpecification | EmptyList = []
    stars: StarList | Literal[Star.TWO_STAR, Star.THREE_STAR] = [2, 3]
    has_structure: StrictBool | None = None

    @field_validator("stars")
    @classmethod
    def validate_star(cls, stars: StarList):
        if not isinstance(stars, list):
            stars = [stars]
        for item in stars:
            assert item in [Star.TWO_STAR, Star.THREE_STAR], "You can search only compounds with 2 or 3 stars"
        return stars

    @model_validator(mode="before")
    @classmethod
    def check_unexpected_fields(cls, data):
        if isinstance(data, dict):
            specifications = cls.model_fields.keys()
            input_specifications = data.keys()
            # if set(specifications).intersection(set(input_specifications)) == set():
            #     raise ValueError("You must include at least one specification in your body request")

            for input_spec in input_specifications:
                if input_spec not in specifications:
                    raise ValueError(
                        f"Your body request has wrong values, valid keys are only: {', '.join(specifications)}"
                    )
        return data
