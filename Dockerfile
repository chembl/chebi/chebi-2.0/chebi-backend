# syntax=docker/dockerfile:1
FROM python:3.12 AS base
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /chebi_backend
COPY requirements.txt /chebi_backend/
RUN pip install -r requirements.txt
COPY . /chebi_backend/
# Temporary patch, move collectstatic to static storage
RUN python manage.py collectstatic --no-input
EXPOSE 8000

FROM base AS django_dev_server
# copy webservices code and config files
ENTRYPOINT [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]

FROM django_dev_server AS gunicorn_server
# copy gunicorn and config files
ENTRYPOINT ["gunicorn", "--bind", ":8000", "--workers", "3", "--timeout", "120", "base.wsgi:application"]