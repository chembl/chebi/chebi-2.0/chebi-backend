# ChEBI Backend

Django based backend for ChEBI2.0 using Python 3.12. Follow this guide to setup the project and to contribute to it.

## Run Locally

This project can be setup locally either via Docker or virtual-env. Follow these common steps:

- Clone the repository

- Create a new file `base/.env` and copy contents of `env.template` to it.

- Setup a new postgres database and add its credentials to your `.env` file.

- While running locally without Docker, you might need to install `HDF5` as it is needed for fpsim2. Install it via brew or apt.

#### Run via Docker

- Build the Docker image locally
    ```bash
    docker build -t chebi-backend .
    ```
- Apply migrations
    ```bash
    docker run --env-file base/.env chebi-backend sh -c "python manage.py migrate"
    ```
- Run the backend at port 8000
    ```bash
    docker run -p 8000:8000 --env-file base/.env chebi-backend
    ```

#### Run via Virtual-env

- Install a Python `3.10` virtual environment (Recommended: use https://github.com/pyenv/pyenv-virtualenv)

- Apply database migrations

    ```bash
    python manage.py migrate
    ```

- Start the server

    ```bash
    python manage.py runserver
    ```

Backend should be all set and running at localhost:8000

## Deployment

Auto deployment is configured via GitLab CI. Any changes pushed to main branch trigger the pipeline which builds the
container and deploys it.

Just incase, if a manual deployment needs to be trigerred locally follow these steps.

#### 1- Build Docker

`docker build --platform linux/amd64 -t dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-backend .`

#### 2- Push container

`docker push dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-backend`

#### 3- Deploy to K8

`kubectl apply -f k8s-deployment.yaml -n chebi-staging`

When running via Kubernetes, secrets are provided via K8 secrets manager. Follow the env.template file and create a k8
secrets configuration accordingly.

## Generate Dump for ETL pipeline

Use following command in order to create a new database dump for ETL

```
pg_dump --column-inserts --no-owner -s -n public -f chebi-schema.sql <database_name>
```
And then generate a dump for inserting Django migrations data
```
pg_dump --column-inserts --no-owner -a -n public -f migrations.sql -t django_migrations <database_name>
```

## Import Database

Use following commands to download copy of public schema for local development

```
pg_dump --format=custom --no-owner -f "chebi.dump" --dbname=<> --schema=<> --username=<>> --host=<>
```

And then import that dump file to local database
```
pg_restore --no-owner -h localhost -p 5432 -U <username> -d <db_name> chebi.dump
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

As a quick start, copy contents of `env.template` and paste it in `base/.env`. Some environment variables are set with a
default value, they will most likely to remain same for local development.

#### Required Environment Variables:

Configure `DB_NAME`, `DB_USER`, and `DB_PASSWORD` according to your database credentials.

## Unit tests

To run unit tests locally is necessary following these steps.
1. Set `ES_ALIAS` to `chebi-subset-compounds`.
2. Run the local server: `python manage.py runserver`
3. Open other terminal an execute `pytest`.

_**NOTE**: The above steps assume you have a local elasticsearch instance with all documents already created and based on our ontology for testing
`http://ftp.ebi.ac.uk/pub/databases/chebi-2/unit_testing/chebi_for_testing.owl`_


## Contributing

Please setup and install `pre-commit` in order to adhere to the project's style guidelines.

Either install globally via `brew install pre-commit` or locally via pip as `pip install pre-commit`

Project's style guide is configured in `.pre-commit-config.yaml`
