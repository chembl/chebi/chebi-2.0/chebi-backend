from django.contrib.auth.views import PasswordResetView as BasePasswordResetView
from django.urls import reverse
from django.views.generic import CreateView, TemplateView

from chebi_models.models import User

from .forms import PasswordResetForm, SignupForm


class PasswordResetView(BasePasswordResetView):
    form_class = PasswordResetForm


class SignupView(CreateView):
    model = User
    template_name = "registration/signup.html"
    form_class = SignupForm

    def get_success_url(self):
        return reverse("signup-request-done")

    def get_form_kwargs(self):
        """Pass request parameters as keyword arguments, it is needed to construct URL"""
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class SignupRequestDoneView(TemplateView):
    template_name = "registration/signup_request_done.html"
