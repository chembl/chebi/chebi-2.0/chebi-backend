from django.contrib import admin
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from utils import send_email

from .models import SubmitterAuthorisation


@admin.register(SubmitterAuthorisation)
class SubmitterToolRequest(admin.ModelAdmin):
    change_form_template = "registration/admin_approve_form.html"
    list_display = ("email", "first_name", "last_name", "affiliation")
    fields = ["first_name", "last_name", "public_name", "affiliation", "address", "is_anonymous"]
    search_fields = ("email", "first_name", "last_name", "affiliation")

    def get_queryset(self, request):
        """
        Only filters those accounts that are inactive and never logged in.
        We assume these accounts are the submission tool requests
        """
        qs = super().get_queryset(request)
        return qs.filter(is_active=False, last_login=None)

    def response_change(self, request, obj):
        if "approve_user" in request.POST:
            self._approve_user(request, obj)
        if "request_details" in request.POST:
            self._request_mode_details(request, obj)
        return super().response_change(request, obj)

    def _approve_user(self, request, obj):
        obj.is_active = True
        obj.save(update_fields=["is_active"])
        self.message_user(request, "Submitter account has been approved.")
        self.log_change(request, obj, f"Submitter Account Approved by {request.user}")
        context = {
            "domain": get_current_site(request).domain,
            "uid": urlsafe_base64_encode(force_bytes(obj.pk)),
            "user": obj,
            "token": default_token_generator.make_token(obj),
            "protocol": "https" if request.is_secure() else "http",
        }
        send_email(
            subject="ChEBI Submission Portal Account Approved",
            body=loader.render_to_string("registration/emails/submitter_account_approved.html", context),
            to=obj.email,
        )

    def _request_mode_details(self, request, obj):
        self.message_user(request, "Email sent to user requesting more details.")
        self.log_change(
            request,
            obj,
            f"{request.user} has initiated a request for clarifications from the user. An "
            f"email has been sent to user.",
        )
        send_email(
            subject="ChEBI Submission Portal - Need More Details",
            body=loader.render_to_string("registration/emails/submitter_account_request_details.html", {"user": obj}),
            to=obj.email,
        )
