from django.urls import include, path

from authentication.views import PasswordResetView, SignupRequestDoneView, SignupView

urlpatterns = [
    path("password_reset/", PasswordResetView.as_view(), name="password_reset"),
    path("", include("django.contrib.auth.urls")),
    path("signup/", SignupView.as_view(), name="signup"),
    path("signup/done", SignupRequestDoneView.as_view(), name="signup-request-done"),
]
