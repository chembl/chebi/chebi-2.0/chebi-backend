from chebi_models.models import User


class SubmitterAuthorisation(User):
    """
    A proxy model of Auth.User, used to approve submitter account requests
    """

    class Meta:
        proxy = True
        verbose_name = "Submitter Portal Request"
