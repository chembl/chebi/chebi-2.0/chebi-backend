from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.template import loader
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from chebi_models.models import User
from utils import send_email


class PasswordResetForm(forms.Form):
    email = forms.EmailField(
        max_length=254,
        widget=forms.EmailInput(attrs={"autocomplete": "email"}),
    )

    def save(self, request, token_generator, *args, **kwargs):
        email = self.cleaned_data["email"]
        user = User.objects.filter(email=email).first()
        if user:
            if not user.is_active:
                messages.error(
                    request,
                    message="Your account is disabled. If you have signed up recently, please wait for your account "
                    "to be approved.",
                )
            else:
                context = {
                    "domain": get_current_site(request).domain,
                    "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                    "user": user,
                    "token": token_generator.make_token(user),
                    "protocol": "https" if request.is_secure() else "http",
                }
                subject = "ChEBI - Password Reset"
                body = loader.render_to_string(
                    template_name="registration/emails/password_reset_email.html", context=context
                )
                send_email(subject=subject, body=body, to=user.email)


class SignupForm(forms.ModelForm):
    address = forms.CharField(label="Organisation Address")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit)
        # Send email to requester
        send_email(
            subject="ChEBI Submission Portal Request Received",
            body=loader.render_to_string(
                template_name="registration/emails/signup_request_received_user.html", context={"user": instance}
            ),
            to=instance.email,
        )
        # Send email to admin
        send_email(
            subject="ChEBI Submitter Authorisation",
            body=loader.render_to_string(
                template_name="registration/emails/signup_request_received_admin.html",
                context={
                    "user": instance,
                    "domain": get_current_site(self.request).domain,
                    "protocol": "https" if self.request.is_secure() else "http",
                    "detail_url": reverse(
                        viewname="admin:authentication_submitterauthorisation_change", args=[instance.pk]
                    ),
                },
            ),
            to=settings.CONTACT_EMAIL_TO,
        )

        return instance

    class Meta:
        model = User
        fields = ["first_name", "last_name", "public_name", "is_anonymous", "email", "affiliation", "address"]
